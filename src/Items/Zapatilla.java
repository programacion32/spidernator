package Items;

import entorno.Entorno;
import entorno.Herramientas;
import java.awt.Image;
import java.util.Random;

public class Zapatilla {

    private double x;
    private double y;
    private double radio;
    private Image zapatilla;

    private long tiempoZapatillaAnterior;
    private long tiempoEspera;
    private boolean estadoZapatilla; 
   

    public Zapatilla(long tiempoEspera) {
        this.estadoZapatilla = false;
        this.tiempoEspera = tiempoEspera;
    }

    public Zapatilla(int x, int y, long tiempoAnterior, long tiempoEspera) {
        this.x = x;
        this.y = y;
        this.radio = 20;
        this.tiempoEspera = tiempoEspera;
        this.tiempoZapatillaAnterior = tiempoAnterior;
        this.estadoZapatilla = true;
        this.zapatilla = Herramientas.cargarImagen("zapatilla.png");
    }

    public void dibujarZapatilla(Entorno e) {
        if (this.estadoZapatilla) {
            e.dibujarImagen(zapatilla, x, y, 0, 0.3);
        }
    }

    public Zapatilla invocarZapatilla(Entorno entorno) {
        Random random = new Random();
        if (System.currentTimeMillis() / 100 > (this.tiempoZapatillaAnterior + this.tiempoEspera)) {
            this.tiempoZapatillaAnterior = System.currentTimeMillis() / 100;
            return new Zapatilla(random.nextInt(entorno.ancho() - 150) + 150, random.nextInt(entorno.alto() - 150) + 150, this.tiempoZapatillaAnterior, this.tiempoEspera);        }
        return this;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    public void setEstadoZapatilla(boolean estadoZapatilla) {
        this.estadoZapatilla = estadoZapatilla;
    }

    public boolean isEstadoZapatilla() {
        return estadoZapatilla;
    }
    

}
