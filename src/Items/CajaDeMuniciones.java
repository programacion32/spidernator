package Items;

import entorno.Entorno;
import entorno.Herramientas;
import java.awt.Image;
import java.util.Random;

public class CajaDeMuniciones {

    private double x;
    private double y;
    private double radio;
    private Image cajaDeMunicion;

    private long tiempoCajaAnterior;
    private long tiempoEspera;
    private boolean estadoCaja;

    public CajaDeMuniciones(long tiempoEspera) {
        this.estadoCaja = false;
        this.tiempoEspera = tiempoEspera;
    }

    public CajaDeMuniciones(int x, int y, long tiempoAnterior, long tiempoEspera) {
        this.x = x;
        this.y = y;
        this.radio = 35;
        this.tiempoEspera = tiempoEspera;
        this.tiempoCajaAnterior = tiempoAnterior;
        this.estadoCaja = true;
        this.cajaDeMunicion = Herramientas.cargarImagen("municion.png");
    }

    public void dibujarCajaDeMunicion(Entorno e) {
        if (this.estadoCaja) {
            e.dibujarImagen(cajaDeMunicion, x, y, 0, 0.35);
        }
    }
    
    public CajaDeMuniciones invocarCajaMuniciones(Entorno entorno){
        Random random = new Random();
        if (System.currentTimeMillis() / 100 > this.tiempoCajaAnterior + this.tiempoEspera) {
            this.tiempoCajaAnterior = System.currentTimeMillis() / 100;
            return new CajaDeMuniciones(random.nextInt(entorno.ancho() - 150) + 150,random.nextInt(entorno.alto() - 150) + 150,this.tiempoCajaAnterior,this.tiempoEspera);
        }
        return this;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }
    
    public void setEstadoCaja(boolean estadoCaja) {
        this.estadoCaja = estadoCaja;
    }

    public boolean isEstadoCaja() {
        return estadoCaja;
    }
    
    

}
