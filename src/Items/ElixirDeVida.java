package Items;

import entorno.Entorno;
import entorno.Herramientas;
import java.awt.Image;
import java.util.Random;

public class ElixirDeVida {

    private double x;
    private double y;
    private double radio;
    private Image elixirDeVida;

    private long tiempoElixirAnterior;
    private long tiempoEspera;
    private boolean estadoElixir; 
   

    public ElixirDeVida(long tiempoEspera) {
        this.estadoElixir = false;
        this.tiempoEspera = tiempoEspera;
    }

    public ElixirDeVida(int x, int y, long tiempoAnterior, long tiempoEspera) {
        this.x = x;
        this.y = y;
        this.radio = 20;
        this.tiempoEspera = tiempoEspera;
        this.tiempoElixirAnterior = tiempoAnterior;
        this.estadoElixir = true;
        this.elixirDeVida = Herramientas.cargarImagen("vida.png");
    }

    public void dibujarElixirDeVida(Entorno e) {
        if (this.estadoElixir) {
            e.dibujarImagen(elixirDeVida, x, y, 0, 0.10);
        }
    }

    public ElixirDeVida invocarElixir(Entorno entorno) {
        Random random = new Random();
        if (System.currentTimeMillis() / 100 > (this.tiempoElixirAnterior + this.tiempoEspera)) {
            this.tiempoElixirAnterior = System.currentTimeMillis() / 100;
            return new ElixirDeVida(random.nextInt(entorno.ancho() - 150) + 150, random.nextInt(entorno.alto() - 150) + 150, this.tiempoElixirAnterior, this.tiempoEspera);        }
        return this;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    public void setEstadoElixir(boolean estadoElixir) {
        this.estadoElixir = estadoElixir;
    }

    public boolean isEstadoElixir() {
        return estadoElixir;
    }
    

}
