package Enemigo;

import entorno.Entorno;
import entorno.Herramientas;
import juego.Exterminador;

import java.awt.Image;
import java.util.Random;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

import Armas.Arma;
import Nivel.Edificio;

public class Enemigo {

    private double x;
    private double y;
    private double xOrigen;
    private double yOrigen;
    private double xAnterior;
    private double yAnterior;

    private double radio;
    private double angulo;
    private double velocidad;
    private double factorVelocidad;// dificultad
    private double escalaImagen;
    private double vida;
    private int exterMasCercano = 0;
    private int danio;

    private Arma telarania;
    private Image imagen;

    public Clip daniado;

    public Enemigo(double x, double y, int tipo) {
        this.x = x;
        this.y = y;
        this.xOrigen = x;
        this.yOrigen = y;

        this.daniado = entorno.Herramientas.cargarSonido("enemigoDaniado.wav");

        switch (tipo) {
            case 0: // Bicho basico
                this.vida = 3;
                this.velocidad = 1;
                this.danio = 3;

                this.radio = 20;
                this.factorVelocidad = 1;

                this.escalaImagen = 0.5;
                this.imagen = Herramientas.cargarImagen("bicho-04.png");
                break;

            case 1: // Tank
                this.vida = 8;
                this.velocidad = 0.5;
                this.danio = 5;

                this.radio = 32;
                this.factorVelocidad = 1;
                this.escalaImagen = 0.4;
                this.imagen = Herramientas.cargarImagen("bicho-01.png");
                break;

            case 2: // Bicho rapido
                this.vida = 1;
                this.velocidad = 1.5;
                this.danio = 6;

                this.radio = 20;
                this.factorVelocidad = 1;
                this.escalaImagen = 0.5;
                this.imagen = Herramientas.cargarImagen("bicho-06.png");

                break;
        }

    }

    // __ __ _____ _____ __ __ ___ _____ _ _ _____ ___
    // | \/ |/ _ \ \ / /_ _| \/ |_ _| ____| \ | |_ _/ _ \
    // | |\/| | | | \ \ / / | || |\/| || || _| | \| | | || | | |
    // | | | | |_| |\ V / | || | | || || |___| |\ | | || |_| |
    // |_| |_|\___/ \_/ |___|_| |_|___|_____|_| \_| |_| \___/
    public void mover() {
        this.xAnterior = x;
        this.yAnterior = y;
        x += velocidad * factorVelocidad * Math.cos(angulo);
        y += velocidad * factorVelocidad * Math.sin(angulo);
    }

    public void moverAtras() {
        x += velocidad * 2 * factorVelocidad * Math.cos(angulo + Math.PI);
        y += velocidad * 2 * factorVelocidad * Math.sin(angulo + Math.PI);
    }

    // MOVER RODEANDO EDIFICIO
    public void moverRodeando(Edificio[] edificio, Exterminador[] exterminador) {
        int factorDesplazamiento = 2;
        boolean exterminadoresVivos = false;

        for (int i = 0; i < exterminador.length; i++) {
            if (exterminador[i] != null) {
                exterminadoresVivos = true;
            }
        }
        if (exterminadoresVivos) {
            for (int i = 0; i < edificio.length; i++) {

                if (chocasteCon(edificio[i])) {
                    double posEdificioXDerecha = (edificio[i].getX() + edificio[i].getAncho()
                            - (exterminador[exterMasCercano].getX() + (exterminador[exterMasCercano].getBase() / 2)));
                    double posEdificioXIzquierda = (exterminador[exterMasCercano].getX()
                            - (exterminador[exterMasCercano].getBase() / 2)
                            - (edificio[i].getX() - edificio[i].getAncho()));

                    double posEdificioYArriba = (edificio[i].getY() + edificio[i].getAlto()
                            - (exterminador[exterMasCercano].getY() + (exterminador[exterMasCercano].getAltura() / 2)));
                    double posEdificioYAbajo = (exterminador[exterMasCercano].getY()
                            - (exterminador[exterMasCercano].getAltura() / 2)
                            - (edificio[i].getY() - edificio[i].getAlto()));

                    if (x + (radio) - factorDesplazamiento <= (edificio[i].getX() - (edificio[i].getAncho() / 2))) {
                        x += 0;
                        this.xAnterior = x;
                        this.yAnterior = y;

                        if (exterminador[exterMasCercano].getX() < x) {// se mueve normal si el jugador esta fuera de la
                            // colision
                            buscarJugador(exterminador);
                            mover();
                        } else if (posEdificioYArriba < posEdificioYAbajo) // se muevecon la condicion de la colision
                        {
                            y += velocidad * factorVelocidad * Math.sin(Math.PI / 2);// AVANZA HACIA ARRIBA
                            angulo = (Math.PI / 2);
                        } else {
                            y += velocidad * factorVelocidad * Math.sin(Math.PI * 3 / 2);// AVANZA HACIA
                            angulo = (Math.PI * 3 / 2);
                        }
                    } else if (x - (radio)
                            + factorDesplazamiento >= (edificio[i].getX() + (edificio[i].getAncho() / 2))) {
                        x += 0;
                        this.xAnterior = x;
                        this.yAnterior = y;

                        if (exterminador[exterMasCercano].getX() > x) {
                            buscarJugador(exterminador);
                            mover();
                        } else if (posEdificioYArriba < posEdificioYAbajo) {
                            y += velocidad * factorVelocidad * Math.sin(Math.PI / 2);
                            angulo = (Math.PI / 2);
                        } else {
                            y += velocidad * factorVelocidad * Math.sin(Math.PI * 3 / 2);
                            angulo = (Math.PI * 3 / 2);
                        }
                    } else if (y - (radio)
                            + factorDesplazamiento >= (edificio[i].getY() + (edificio[i].getAlto() / 2))) {
                        y += 0;
                        this.xAnterior = x;
                        this.yAnterior = y;

                        if (exterminador[exterMasCercano].getY() > y) {
                            buscarJugador(exterminador);
                            mover();
                        } else if (posEdificioXIzquierda > posEdificioXDerecha) {
                            x += velocidad * factorVelocidad * Math.cos(0);
                            angulo = 0;
                        } else {
                            x += velocidad * factorVelocidad * Math.cos(Math.PI);
                            angulo = Math.PI;
                        }
                    } else if (y + (radio)
                            - factorDesplazamiento <= (edificio[i].getY() - (edificio[i].getAlto() / 2))) {
                        y += 0;
                        this.xAnterior = x;
                        this.yAnterior = y;

                        if (exterminador[exterMasCercano].getY() < y) {
                            buscarJugador(exterminador);
                            mover();
                        } else if (posEdificioXIzquierda > posEdificioXDerecha) {
                            x += velocidad * factorVelocidad * Math.cos(0);
                            angulo = 0;
                        } else {
                            x += velocidad * factorVelocidad * Math.cos(Math.PI);
                            angulo = Math.PI;
                        }
                    } else {
                        x = xAnterior;
                        y = yAnterior;
                    }
                }
            }
        }
    }

    public void daniado() throws LineUnavailableException {
        daniado.setFramePosition(0);
        daniado.start();
    }

    // MOVER RODEANDO ENEMIGOS
    public void moverEnColision(Enemigo enemigo) {
        int factorDesplazamiento = 3;

        if (x + (radio) - factorDesplazamiento < (enemigo.getX() - (enemigo.getRadio()))) { // Choca Izquierda
            x = (enemigo.getX() - enemigo.getRadio()) - (radio);
        } else if (x - (radio) + factorDesplazamiento >= (enemigo.getX() + (enemigo.getRadio()))) { // Choca Derecha
            x = (enemigo.getX() + enemigo.getRadio()) + (radio);
        } else if (y - (radio) + factorDesplazamiento >= (enemigo.getY() + (enemigo.getRadio()))) { // Choca Abajo
            y = (enemigo.getY() + enemigo.getRadio()) + (radio);
        } else if (y + (radio) - factorDesplazamiento <= (enemigo.getY() - (enemigo.getRadio()))) { // Choca Arriba
            y = (enemigo.getY() - enemigo.getRadio()) - (radio);
        }
    }

    // DEVUELVE EL NUMERO DE ENEMIGO CONTRA EL QUE COLISIONO
    public int cualChocaste(Enemigo[] enemigo) {
        for (int nEn = 0; nEn < enemigo.length; nEn++) {
            if (this.chocasteCon(enemigo[nEn])) {
                return nEn;
            }
        }
        return -1;
    }

    private boolean chocasteCon(Enemigo enemigo) {
        if (enemigo != null) {
            return ((x + radio >= enemigo.getX() - enemigo.getRadio())
                    && (x - radio <= enemigo.getX() + enemigo.getRadio())
                    && (y + radio >= enemigo.getY() - enemigo.getRadio())
                    && (y - radio <= enemigo.getY() + enemigo.getRadio()));
        }
        return false;
    }

    // DEVUELVE EL NUMERO DE EDIFICIO CONTRA EL QUE COLISIONO
    public int cualChocaste(Edificio[] edificio) {
        for (int nEd = 0; nEd < edificio.length; nEd++) {
            if (this.chocasteCon(edificio[nEd])) {
                return nEd;
            }
        }
        return -1;
    }

    private boolean chocasteCon(Edificio edificio) {
        if (edificio != null) {
            return (x + (radio) >= (edificio.getX() - (edificio.getAncho() / 2))
                    && x - (radio) <= edificio.getX() + (edificio.getAncho() / 2))
                    && (y + (radio) >= (edificio.getY() - (edificio.getAlto() / 2))
                    && y - (radio) <= edificio.getY() + (edificio.getAlto() / 2));
        }
        return false;
    }

    public void dibujar(Entorno e) {
        // e.dibujarCirculo(x, y, radio*2, Color.yellow);
        e.dibujarImagen(imagen, x, y, angulo + Math.PI / 2, escalaImagen);
    }

    // OBTIENE EL ANGULO DEL JUGADOR MAS CERCANO
    public void buscarJugador(Exterminador[] j) {

        double[] ady = new double[j.length];
        double[] op = new double[j.length];
        double[] hyp = new double[j.length];
        double menor = 0;
        this.exterMasCercano = 1;
        boolean exterminadoresVivos = false;

        for (int i = 0; i < j.length; i++) {
            if (j[i] != null) {
                exterminadoresVivos = true;
                ady[i] = j[i].getX() - x;
                op[i] = j[i].getY() - y;
                hyp[i] = Math.sqrt(op[i] * op[i] + ady[i] * ady[i]);
                if (i == 0) {
                    menor = hyp[i];
                    this.exterMasCercano = i;
                } else if (menor > hyp[i]) {
                    menor = hyp[i];
                    this.exterMasCercano = i;
                }
            }
        }
        if (exterminadoresVivos) {
            if (this.exterMasCercano != -1) {
                if (x < j[this.exterMasCercano].getX()) {
                    angulo = Math.asin(op[this.exterMasCercano] / hyp[this.exterMasCercano]);
                } else {
                    angulo = -Math.asin(op[this.exterMasCercano] / hyp[this.exterMasCercano]) + Math.PI;
                }
            }
        } else {
            this.velocidad = 0.5;
            this.huir();
        }
    }

    public void huir() {
        double ady = xOrigen - x;
        double op = yOrigen - y;
        double hyp = Math.sqrt(op * op + ady * ady);
        if (x < xOrigen) {
            angulo = Math.asin(op / hyp);
        } else {
            angulo = -Math.asin(op / hyp) + Math.PI;
        }
    }

    public Arma dispararArma() {
        Random random = new Random();
        telarania = new Arma();
        boolean noHayDisparos = true;
        if (random.nextInt(500) == 0) {
            telarania.setTelaArania(x, y, angulo);
            noHayDisparos = false;
        }
        if (noHayDisparos) {
            return null;
        } else {
            return telarania;
        }
    }
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setAncho(double ancho) {
        this.radio = ancho;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }

    public double getAngulo() {
        return angulo;
    }

    public void setAngulo(double angulo) {
        this.angulo = angulo;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public double getFactorVelocidad() {
        return factorVelocidad;
    }

    public void setFactorVelocidad(double factorVelocidad) {
        this.factorVelocidad = factorVelocidad;
    }

    public double getVida() {
        return vida;
    }

    public void quitarVida(double danio) {
        this.vida -= danio;
    }

    public int getDanio() {
        return danio;
    }

    public void setDanio(int danio) {
        this.danio = danio;
    }

}
