package Enemigo;

import java.awt.Point;
import entorno.Entorno;

import java.util.Random;

import Enemigo.Enemigo;

public class GeneradorDeEnemigos {

    private long tiempoSpawnAnterior = 0;
    private Point[] puntos = new Point[12];
    private Enemigo enemigo;

    public GeneradorDeEnemigos(Entorno entorno) {
        puntosDeSpawneo(entorno);
    }

    private void puntosDeSpawneo(Entorno entorno) {

        this.puntos[0] = new Point(-40, 128);
        this.puntos[1] = new Point(-40, 280);
        this.puntos[2] = new Point(-40, 460);

        this.puntos[3] = new Point(entorno.ancho() + 40, 128);
        this.puntos[4] = new Point(entorno.ancho() + 40, 280);
        this.puntos[5] = new Point(entorno.ancho() + 40, 460);

        this.puntos[6] = new Point(288, -40);
        this.puntos[7] = new Point(528, -40);
        this.puntos[8] = new Point(790, -40);

        this.puntos[9] = new Point(288, entorno.alto() + 40);
        this.puntos[10] = new Point(528, entorno.alto() + 40);
        this.puntos[11] = new Point(790, entorno.alto() + 40);

    }

    private Point spawnearEnemigo() {
        Random random = new Random();
        Point puntoElegido = puntos[random.nextInt(puntos.length)];
        return puntoElegido;

    }

    public Enemigo crearEnemigos() {
        Point punto = spawnearEnemigo();
        Random random = new Random();
        this.enemigo = new Enemigo(punto.getX(), punto.getY(), random.nextInt(3));
        return this.enemigo;

    }
}
