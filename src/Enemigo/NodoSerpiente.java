package Enemigo;

public class NodoSerpiente {

    public double x;
    public double y;
    public double radio;
    public double angulo;

    public NodoSerpiente(int posX, int posY, double radio, double angulo) {
        this.x = posX;
        this.y = posY;
        this.radio = radio;
        this.angulo = angulo;

    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    public double getAngulo() {
        return angulo;
    }

    public void setAngulo(double angulo) {
        this.angulo = angulo;
    }

}
