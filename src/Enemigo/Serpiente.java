
package Enemigo;

import java.awt.Color;
import java.awt.Image;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

import Armas.Arma;
import Nivel.Edificio;
import entorno.Entorno;
import entorno.Herramientas;
import juego.Exterminador;

public class Serpiente {

    private NodoSerpiente[] nodosSerpiente;
    private double velocidad;
    private int exterMasCercano;
    private double xAnterior;
    private double yAnterior;
    private int nodoCola;
    private boolean vivo;

    private Image cola;
    private Image tronco;
    private Image cabeza;

    private Image emoticonFeliz;
    private Image emoticonEnojado;

    private boolean golpeado;
    private int danio;

    private Clip daniado;

    public Serpiente(int posX, int posY, double radio, double angulo, double velocidad, int largoDeSerpiente) {
        nodosSerpiente = new NodoSerpiente[largoDeSerpiente];
        this.cola = Herramientas.cargarImagen("SerpienteCola.png");
        this.tronco = Herramientas.cargarImagen("SerpienteTronco.png");
        this.cabeza = Herramientas.cargarImagen("SerpienteCabeza.png");

        this.emoticonEnojado = Herramientas.cargarImagen("emoteEnojado.png");
        this.emoticonFeliz = Herramientas.cargarImagen("emoteFeliz.png");

        this.nodoCola = 0;
        this.velocidad = velocidad;

        this.vivo = true;
        this.golpeado = false;
        this.danio = 1;

        this.daniado = entorno.Herramientas.cargarSonido("enemigoDaniado.wav");

        for (int i = 0; i < nodosSerpiente.length; i++) {
            nodosSerpiente[i] = new NodoSerpiente(posX, posY, radio, angulo);
        }

    }

    public void daniado() throws LineUnavailableException {
        daniado.setFramePosition(0);
        daniado.start();
    }

    public void mover() {
        if (this.vivo) {
            int primeroVivo = 0;
            for (int i = 0; i < nodosSerpiente.length; i++) {
                if (nodosSerpiente[i] != null) {
                    primeroVivo = i;
                }
            }

            for (int i = 0; i < nodosSerpiente.length; i++) {
                if (nodosSerpiente[i] != null) {
                    if (i < nodosSerpiente.length - 1) {
                        double ady = nodosSerpiente[i].getX() - nodosSerpiente[i + 1].getX();
                        double op = nodosSerpiente[i].getY() - nodosSerpiente[i + 1].getY();
                        double hyp = Math.sqrt(op * op + ady * ady);
                        if (hyp > nodosSerpiente[primeroVivo].getRadio()) {
                            if (nodosSerpiente[i].getX() < nodosSerpiente[i + 1].getX()) {
                                nodosSerpiente[i].setAngulo(-Math.asin(op / hyp));
                            } else {
                                nodosSerpiente[i].setAngulo(Math.asin(op / hyp) + Math.PI);

                            }

                            nodosSerpiente[i].setX(
                                    nodosSerpiente[i].getX() + (velocidad * Math.cos(nodosSerpiente[i].getAngulo())));
                            nodosSerpiente[i].setY(
                                    nodosSerpiente[i].getY() + (velocidad * Math.sin(nodosSerpiente[i].getAngulo())));
                        }

                    } else {
                        nodosSerpiente[nodosSerpiente.length - 1].setX(nodosSerpiente[nodosSerpiente.length - 1].getX()
                                + (velocidad * Math.cos(nodosSerpiente[nodosSerpiente.length - 1].getAngulo())));
                        nodosSerpiente[nodosSerpiente.length - 1].setY(nodosSerpiente[nodosSerpiente.length - 1].getY()
                                + (velocidad * Math.sin(nodosSerpiente[nodosSerpiente.length - 1].getAngulo())));

                    }
                    if (nodosSerpiente[nodosSerpiente.length - 1] != null) {
                        this.xAnterior = nodosSerpiente[nodosSerpiente.length - 1].getX();
                        this.yAnterior = nodosSerpiente[nodosSerpiente.length - 1].getY();
                    }
                }
            }
        }
    }

    public void moverSinCabeza() {
        if (this.vivo) {
            for (int i = this.nodoCola; i < nodosSerpiente.length; i++) {
                if (i < nodosSerpiente.length - 1) {
                    double ady = nodosSerpiente[i].getX() - nodosSerpiente[i + 1].getX();
                    double op = nodosSerpiente[i].getY() - nodosSerpiente[i + 1].getY();
                    double hyp = Math.sqrt(op * op + ady * ady);
                    if (hyp > nodosSerpiente[this.nodoCola].getRadio()) {
                        if (nodosSerpiente[i].getX() < nodosSerpiente[i + 1].getX()) {
                            nodosSerpiente[i].setAngulo(-Math.asin(op / hyp));
                        } else {
                            nodosSerpiente[i].setAngulo(Math.asin(op / hyp) + Math.PI);

                        }

                        nodosSerpiente[i]
                                .setX(nodosSerpiente[i].getX() + (velocidad * Math.cos(nodosSerpiente[i].getAngulo())));
                        nodosSerpiente[i]
                                .setY(nodosSerpiente[i].getY() + (velocidad * Math.sin(nodosSerpiente[i].getAngulo())));
                    }

                }
            }
        }
    }

    // OBTIENE EL ANGULO DEL JUGADOR MAS CERCANO
    public void buscarJugador(Exterminador[] j) {
        if (this.vivo) {
            double[] ady = new double[j.length];
            double[] op = new double[j.length];
            double[] hyp = new double[j.length];
            double menor = 0;
            this.exterMasCercano = 1;
            boolean exterminadoresVivos = false;

            for (int i = 0; i < j.length; i++) {
                if (j[i] != null) {
                    exterminadoresVivos = true;
                    ady[i] = j[i].getX() - nodosSerpiente[nodosSerpiente.length - 1].getX();
                    op[i] = j[i].getY() - nodosSerpiente[nodosSerpiente.length - 1].getY();
                    hyp[i] = Math.sqrt(op[i] * op[i] + ady[i] * ady[i]);
                    if (i == 0) {
                        menor = hyp[i];
                        this.exterMasCercano = i;
                    } else if (menor > hyp[i]) {
                        menor = hyp[i];
                        this.exterMasCercano = i;
                    }
                }
            }
            if (exterminadoresVivos) {
                if (this.exterMasCercano != -1) {
                    if (nodosSerpiente[nodosSerpiente.length - 1].getX() < j[this.exterMasCercano].getX()) {
                        nodosSerpiente[nodosSerpiente.length - 1]
                                .setAngulo(Math.asin(op[this.exterMasCercano] / hyp[this.exterMasCercano]));
                    } else {
                        nodosSerpiente[nodosSerpiente.length - 1]
                                .setAngulo(-Math.asin(op[this.exterMasCercano] / hyp[this.exterMasCercano]) + Math.PI);

                    }
                }
            }
        }
    }

    public boolean estadoSerpiente() {
        for (int i = this.nodoCola; i < nodosSerpiente.length; i++) {
            if (nodosSerpiente[i] != null) {
                this.nodoCola = i;
                break;
            }
        }

        if (nodoCola == nodosSerpiente.length - 1) {
            this.vivo = false;
        }
        return vivo;
    }

    public void dibujar(Entorno e) {
        if (this.vivo) {
            if (nodoCola != nodosSerpiente.length);
            for (int i = this.nodoCola + 1; i < nodosSerpiente.length - 1; i++) {

                e.dibujarImagen(tronco, nodosSerpiente[i].getX(), nodosSerpiente[i].getY(),
                        nodosSerpiente[i].getAngulo(), 2);
            }
            if (nodoCola != nodosSerpiente.length);
            e.dibujarImagen(cola, nodosSerpiente[this.nodoCola].getX(), nodosSerpiente[this.nodoCola].getY(),
                    nodosSerpiente[this.nodoCola].getAngulo(), 2);

            e.dibujarImagen(cabeza, nodosSerpiente[nodosSerpiente.length - 1].getX(),
                    nodosSerpiente[nodosSerpiente.length - 1].getY(),
                    nodosSerpiente[nodosSerpiente.length - 1].getAngulo(), 2);

            if (golpeado) {
                e.dibujarImagen(emoticonEnojado, nodosSerpiente[nodosSerpiente.length - 1].getX(), nodosSerpiente[nodosSerpiente.length - 1].getY() - 50, 0);
            } else {
                e.dibujarImagen(emoticonFeliz, nodosSerpiente[nodosSerpiente.length - 1].getX(), nodosSerpiente[nodosSerpiente.length - 1].getY() - 50, 0);
            }

        }
    }

    // DEVUELVE EL NUMERO DE EDIFICIO CONTRA EL QUE COLISIONO
    public int cualChocaste(Edificio[] edificio) {
        for (int nEd = 0; nEd < edificio.length; nEd++) {
            if (this.chocasteCon(edificio[nEd])) {
                return nEd;
            }
        }
        return -1;
    }

    private boolean chocasteCon(Edificio edificio) {
        if (this.vivo) {
            if (edificio != null) {
                if (nodosSerpiente[nodosSerpiente.length - 1] != null) {
                    return (nodosSerpiente[nodosSerpiente.length - 1].getX()
                            + (nodosSerpiente[nodosSerpiente.length - 1].getRadio()) >= (edificio.getX()
                            - (edificio.getAncho() / 2))
                            && nodosSerpiente[nodosSerpiente.length - 1].getX()
                            - (nodosSerpiente[nodosSerpiente.length - 1].getRadio()) <= edificio.getX()
                            + (edificio.getAncho() / 2))
                            && (nodosSerpiente[nodosSerpiente.length - 1].getY()
                            + (nodosSerpiente[nodosSerpiente.length - 1].getRadio()) >= (edificio.getY()
                            - (edificio.getAlto() / 2))
                            && nodosSerpiente[nodosSerpiente.length - 1].getY()
                            - (nodosSerpiente[nodosSerpiente.length - 1].getRadio()) <= edificio.getY()
                            + (edificio.getAlto() / 2));
                }
            }
            return false;
        }
        return false;
    }

    public void moverEnColision(Enemigo enemigo) {
        if (this.vivo) {
            int factorDesplazamiento = 3;

            if (nodosSerpiente[nodosSerpiente.length - 1].getX()
                    + (nodosSerpiente[nodosSerpiente.length - 1].getRadio())
                    - factorDesplazamiento < (enemigo.getX() - (enemigo.getRadio()))) { // Choca Izquierda
                nodosSerpiente[nodosSerpiente.length - 1].setX(
                        (enemigo.getX() - enemigo.getRadio()) - (nodosSerpiente[nodosSerpiente.length - 1].getRadio()));
            } else if (nodosSerpiente[nodosSerpiente.length - 1].getX()
                    - (nodosSerpiente[nodosSerpiente.length - 1].getRadio())
                    + factorDesplazamiento >= (enemigo.getX() + (enemigo.getRadio()))) { // Choca Derecha
                nodosSerpiente[nodosSerpiente.length - 1].setX(
                        (enemigo.getX() + enemigo.getRadio()) + (nodosSerpiente[nodosSerpiente.length - 1].getRadio()));
            } else if (nodosSerpiente[nodosSerpiente.length - 1].getY()
                    - (nodosSerpiente[nodosSerpiente.length - 1].getRadio())
                    + factorDesplazamiento >= (enemigo.getY() + (enemigo.getRadio()))) { // Choca Abajo
                nodosSerpiente[nodosSerpiente.length - 1].setY(
                        (enemigo.getY() + enemigo.getRadio()) + (nodosSerpiente[nodosSerpiente.length - 1].getRadio()));
            } else if (nodosSerpiente[nodosSerpiente.length - 1].getY()
                    + (nodosSerpiente[nodosSerpiente.length - 1].getRadio())
                    - factorDesplazamiento <= (enemigo.getY() - (enemigo.getRadio()))) { // Choca Arriba
                nodosSerpiente[nodosSerpiente.length - 1].setY(
                        (enemigo.getY() - enemigo.getRadio()) - (nodosSerpiente[nodosSerpiente.length - 1].getRadio()));
            }
            xAnterior = nodosSerpiente[nodosSerpiente.length - 1].getX();
            yAnterior = nodosSerpiente[nodosSerpiente.length - 1].getY();
        }
    }

    public void moverRodeando(Edificio[] edificio, Exterminador[] exterminador) {
        if (this.vivo) {
            int factorDesplazamiento = 2;
            boolean exterminadoresVivos = false;

            for (int i = 0; i < exterminador.length; i++) {
                if (exterminador[i] != null) {
                    exterminadoresVivos = true;
                }
            }
            if (exterminadoresVivos) {
                for (int i = 0; i < edificio.length; i++) {

                    if (chocasteCon(edificio[i])) {
                        double posEdificioXDerecha = (edificio[i].getX() + edificio[i].getAncho()
                                - (exterminador[exterMasCercano].getX()
                                + (exterminador[exterMasCercano].getBase() / 2)));
                        double posEdificioXIzquierda = (exterminador[exterMasCercano].getX()
                                - (exterminador[exterMasCercano].getBase() / 2)
                                - (edificio[i].getX() - edificio[i].getAncho()));

                        double posEdificioYArriba = (edificio[i].getY() + edificio[i].getAlto()
                                - (exterminador[exterMasCercano].getY()
                                + (exterminador[exterMasCercano].getAltura() / 2)));
                        double posEdificioYAbajo = (exterminador[exterMasCercano].getY()
                                - (exterminador[exterMasCercano].getAltura() / 2)
                                - (edificio[i].getY() - edificio[i].getAlto()));

                        if (nodosSerpiente[nodosSerpiente.length - 1].getX()
                                + (nodosSerpiente[nodosSerpiente.length - 1].getRadio())
                                - factorDesplazamiento <= (edificio[i].getX() - (edificio[i].getAncho() / 2))) {
                            nodosSerpiente[nodosSerpiente.length - 1]
                                    .setX(nodosSerpiente[nodosSerpiente.length - 1].getX());
                            this.xAnterior = nodosSerpiente[nodosSerpiente.length - 1].getX();
                            this.yAnterior = nodosSerpiente[nodosSerpiente.length - 1].getY();

                            System.out.println("izquierda");

                            if (exterminador[exterMasCercano].getX() < nodosSerpiente[nodosSerpiente.length - 1]
                                    .getX()) {
                                buscarJugador(exterminador);
                                mover();
                            } else if (posEdificioYArriba < posEdificioYAbajo) // se muevecon la condicion de la
                            // colision
                            {
                                nodosSerpiente[nodosSerpiente.length - 1]
                                        .setY(nodosSerpiente[nodosSerpiente.length - 1].getY()
                                                + velocidad * Math.sin(Math.PI / 2));
                                nodosSerpiente[nodosSerpiente.length - 1].setAngulo(Math.PI / 2);
                            } else {
                                nodosSerpiente[nodosSerpiente.length - 1]
                                        .setY(nodosSerpiente[nodosSerpiente.length - 1].getY()
                                                + velocidad * Math.sin(Math.PI * 3 / 2));
                                nodosSerpiente[nodosSerpiente.length - 1].setAngulo(Math.PI * 3 / 2);
                            }
                        } else if (nodosSerpiente[nodosSerpiente.length - 1].getX()
                                - (nodosSerpiente[nodosSerpiente.length - 1].getRadio())
                                + factorDesplazamiento >= (edificio[i].getX() + (edificio[i].getAncho() / 2))) {

                            nodosSerpiente[nodosSerpiente.length - 1]
                                    .setX(nodosSerpiente[nodosSerpiente.length - 1].getX());

                            this.xAnterior = nodosSerpiente[nodosSerpiente.length - 1].getX();
                            this.yAnterior = nodosSerpiente[nodosSerpiente.length - 1].getY();

                            System.out.println("derecha");

                            if (exterminador[exterMasCercano].getX() > nodosSerpiente[nodosSerpiente.length - 1]
                                    .getX()) {
                                buscarJugador(exterminador);
                                mover();
                            } else if (posEdificioYArriba < posEdificioYAbajo) {
                                nodosSerpiente[nodosSerpiente.length - 1]
                                        .setY(nodosSerpiente[nodosSerpiente.length - 1].getY()
                                                + velocidad * Math.sin(Math.PI / 2));
                                nodosSerpiente[nodosSerpiente.length - 1].setAngulo(Math.PI / 2);

                            } else {
                                nodosSerpiente[nodosSerpiente.length - 1]
                                        .setY(nodosSerpiente[nodosSerpiente.length - 1].getY()
                                                + velocidad * Math.sin(Math.PI * 3 / 2));
                                nodosSerpiente[nodosSerpiente.length - 1].setAngulo(Math.PI * 3 / 2);
                            }
                        } else if (nodosSerpiente[nodosSerpiente.length - 1].getY()
                                - (nodosSerpiente[nodosSerpiente.length - 1].getRadio())
                                + factorDesplazamiento >= (edificio[i].getY() + (edificio[i].getAlto() / 2))) {
                            nodosSerpiente[nodosSerpiente.length - 1]
                                    .setY(nodosSerpiente[nodosSerpiente.length - 1].getY());

                            this.xAnterior = nodosSerpiente[nodosSerpiente.length - 1].getX();
                            this.yAnterior = nodosSerpiente[nodosSerpiente.length - 1].getY();

                            System.out.println("abajo");

                            if (exterminador[exterMasCercano].getY() > nodosSerpiente[nodosSerpiente.length - 1]
                                    .getY()) {
                                buscarJugador(exterminador);
                                mover();
                            } else if (posEdificioXIzquierda > posEdificioXDerecha) {
                                nodosSerpiente[nodosSerpiente.length - 1].setX(
                                        nodosSerpiente[nodosSerpiente.length - 1].getX() + velocidad * Math.cos(0));
                                nodosSerpiente[nodosSerpiente.length - 1].setAngulo(0);
                            } else {
                                nodosSerpiente[nodosSerpiente.length - 1]
                                        .setX(nodosSerpiente[nodosSerpiente.length - 1].getX()
                                                + velocidad * Math.cos(Math.PI));
                                nodosSerpiente[nodosSerpiente.length - 1].setAngulo(Math.PI);
                            }
                        } else if (nodosSerpiente[nodosSerpiente.length - 1].getY()
                                + (nodosSerpiente[nodosSerpiente.length - 1].getRadio())
                                - factorDesplazamiento <= (edificio[i].getY() - (edificio[i].getAlto() / 2))) {
                            nodosSerpiente[nodosSerpiente.length - 1]
                                    .setY(nodosSerpiente[nodosSerpiente.length - 1].getY());
                            this.xAnterior = nodosSerpiente[nodosSerpiente.length - 1].getX();
                            this.yAnterior = nodosSerpiente[nodosSerpiente.length - 1].getY();

                            System.out.println("arriba");

                            if (exterminador[exterMasCercano].getY() < nodosSerpiente[nodosSerpiente.length - 1]
                                    .getY()) {
                                buscarJugador(exterminador);
                                mover();
                            } else if (posEdificioXIzquierda > posEdificioXDerecha) {
                                nodosSerpiente[nodosSerpiente.length - 1].setX(
                                        nodosSerpiente[nodosSerpiente.length - 1].getX() + velocidad * Math.cos(0));
                                nodosSerpiente[nodosSerpiente.length - 1].setAngulo(0);
                            } else {
                                nodosSerpiente[nodosSerpiente.length - 1]
                                        .setX(nodosSerpiente[nodosSerpiente.length - 1].getX()
                                                + velocidad * Math.cos(Math.PI));
                                nodosSerpiente[nodosSerpiente.length - 1].setAngulo(Math.PI);
                            }
                        } else {
                            System.out.println("hola");
                            nodosSerpiente[nodosSerpiente.length - 1].setX(xAnterior);
                            nodosSerpiente[nodosSerpiente.length - 1].setY(yAnterior);
                            xAnterior = nodosSerpiente[nodosSerpiente.length - 1].getX();
                            yAnterior = nodosSerpiente[nodosSerpiente.length - 1].getY();
                        }
                    }
                }
            }
            moverSinCabeza();
        }
    }

    public NodoSerpiente[] getNodosSerpiente() {
        return nodosSerpiente;
    }

    public void setNodosSerpiente(NodoSerpiente[] nodosSerpiente) {
        this.nodosSerpiente = nodosSerpiente;
    }

    public int getNodoCola() {
        return nodoCola;
    }

    public void setNodoCola(int nodoCola) {
        this.nodoCola = nodoCola;
    }

    public boolean isGolpeado() {
        return golpeado;
    }

    public void setGolpeado(boolean danio) {
        this.golpeado = danio;
    }

    public double getDanio() {
        return danio;
    }

    public void setDanio(double danio) {
        this.danio = (int) danio;
    }

}
