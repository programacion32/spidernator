package juego;

import java.lang.System;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

import Armas.Arma;
import Enemigo.Enemigo;
import Enemigo.Serpiente;
import Items.CajaDeMuniciones;
import Items.ElixirDeVida;
import Items.Zapatilla;
import Nivel.Edificio;
import Enemigo.GeneradorDeEnemigos;
import Nivel.GeneradorDeNiveles;
import Nivel.Interfaz;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

    private Entorno entorno;

    private Edificio edificio[];
    private Exterminador exterminador[];
    private Enemigo enemigo[];
    private Image spriteExterminador[][];
    private GeneradorDeNiveles nivel;
    private GeneradorDeEnemigos genEnemigos;
    private Controles[] controlJugador;
    private Interfaz interfaz;
    private CajaDeMuniciones cajaDeMuniciones;
    private ElixirDeVida elixirDeVida;
    private Zapatilla zapatilla;
    private Serpiente serpiente;

    private Arma[] armasEnJuego;
    private long tiempoEnemigoCreadoAnterior;
    private int factorSpawneo = 50;
    private int puntaje = 0;
    private Image pantallaTitulos;
    private boolean gameStart;

    private int armasDisponiblesParaJugadores = 0;
    private int anuncio = -1;
    private int cantidadJugadores = 1;
    private Clip sonidoBackground;

    Juego() {

        this.entorno = new Entorno(this, "Spiderneitor", 1000, 600);
        reiniciarJuego();
        this.entorno.iniciar();
    }

    public void reiniciarJuego() {
        // INICIA LA INTERFAZ
        gameStart = false;
        interfaz = new Interfaz();

        sonidoBackground = Herramientas.cargarSonido("sonidobackground.wav");
        armasEnJuego = new Arma[100];

        // CREAR EDIFICIOS (SE CREAN POR UNICA VEZ)
        genEnemigos = new GeneradorDeEnemigos(entorno);
        serpiente = null;
        // GENERADOR DE NIVELES
        nivel = new GeneradorDeNiveles();
        edificio = new Edificio[nivel.getEdificios().length];
        edificio = nivel.getEdificios();

        // CREAR E INICIAR ENEMIGOS (LUEGO SE VAN A SEGUIR INVOCANDO EN VIVO)
        enemigo = new Enemigo[10];

        // INICIA LOS ITEMS
        elixirDeVida = new ElixirDeVida(250);// 100 = 10 segundos, 50 = 5 segundos.
        cajaDeMuniciones = new CajaDeMuniciones(150);
        zapatilla = new Zapatilla(100);
        
        factorSpawneo = 50;
        puntaje = 0;
        armasDisponiblesParaJugadores = 0;
        anuncio = -1;
    }

	public void pantallaDeTitulos() {

    	sonidoBackground.loop(Clip.LOOP_CONTINUOUSLY);
        entorno.cambiarFont("CWEBL.TTF", 40, Color.BLUE);
       
        pantallaTitulos = Herramientas.cargarImagen("title.png");
        entorno.dibujarImagen(pantallaTitulos, entorno.ancho() / 2, entorno.alto() / 2, 0);
        //entorno.escribirTexto("Cantidad de Jugadores:", 40, entorno.alto() - 140);
        entorno.escribirTexto("< " + cantidadJugadores + " >", 220, entorno.alto() - 70);

        if (entorno.sePresiono(entorno.TECLA_DERECHA)) {
            if (cantidadJugadores < 2) {
                this.cantidadJugadores++;
            }
        }

        if (entorno.sePresiono(entorno.TECLA_IZQUIERDA)) {
            if (cantidadJugadores > 1) {
                this.cantidadJugadores--;
            }
        }
        if (entorno.sePresiono(entorno.TECLA_ENTER)) {

            // INICIA CANTIDAD DE JUGADORES/CONTROLES
            exterminador = new Exterminador[cantidadJugadores];
            controlJugador = new Controles[exterminador.length];
            // CREAR JUGADORES
            // --------------------------------------------------------------------------------------------
            spriteExterminador = new Image[exterminador.length][4]; // []: cantidad de jugadores []:cantidad de sprites para
            // cadajugador
            int fsuma = 1;
            for (int nExterminador = 0; nExterminador < exterminador.length; nExterminador++) {
                // INICIA LOS CONTROLES
                if (nExterminador == 0) {
                    controlJugador[nExterminador] = new Controles('w', 's', 'd', 'a', 'g', 'f', 'h');
                }
                if (nExterminador == 1) {
                    controlJugador[nExterminador] = new Controles(entorno.TECLA_ARRIBA, entorno.TECLA_ABAJO,
                            entorno.TECLA_DERECHA, entorno.TECLA_IZQUIERDA, entorno.TECLA_SHIFT, '.', '-');
                }

                // INICIA LA POSICION DE LOS EXTERMINADORES
                exterminador[nExterminador] = new Exterminador((entorno.ancho() / 2) + (fsuma * 50), entorno.alto() / 2, 50,
                        50, 0, 2, 500);
                fsuma *= -1;

                // INICIA LOS SPRITES DE LOS EXTERMINIADORES
                for (int nSprite = 0; nSprite < spriteExterminador[nExterminador].length; nSprite++) {
                    spriteExterminador[nExterminador][nSprite] = Herramientas
                            .cargarImagen("Soldadito0" + (nExterminador + 1) + "-0" + (nSprite + 1) + ".png");
                }
            }
            gameStart = true;
        }
    }

    public void mainGame() throws LineUnavailableException {
        

        // crea enemigos a medida que pasa el tiempo, por enemigo matado se acelera 0.1
        // el spawn
        if (serpiente == null) {
            if (System.currentTimeMillis() / 100 > tiempoEnemigoCreadoAnterior + factorSpawneo) {
                boolean enemigoCreado = true;
                for (int i = 0; i < enemigo.length; i++) {
                    if (enemigo[i] == null && enemigoCreado) {
                        enemigoCreado = false;
                        enemigo[i] = genEnemigos.crearEnemigos();
                    }
                }

                tiempoEnemigoCreadoAnterior = System.currentTimeMillis() / 100;
            }
        }
        // DIBUJA EL PISO
        nivel.dibujarSuelo(entorno);

        // DIBUJA LOS EDIFICIOS EN EL NIVEL CON BLOQUES DE 32 PIXELES
        nivel.dibujarParedesEdificios(entorno, 32);

        // COMPORTAMIENTO DE LOS JUGADORES // j = numero de jugador
        for (int j = 0; j < exterminador.length; j++) {
            if (exterminador[j] == null) {
                controlJugador[j] = null;
            }

            if (exterminador[j] != null) {
                // BUSCA LA COLISION CON LAS CAJAS DE MUNICION
                if (exterminador[j].chocasteCon(cajaDeMuniciones)) {
                    exterminador[j].sonido(exterminador[j].getAgarrarItem());
                    exterminador[j].recargarBalas();
                    cajaDeMuniciones.setEstadoCaja(false);
                }

                // BUSCA LA COLISION CON LOS ELIXIR DE VIDA
                if (exterminador[j].chocasteCon(elixirDeVida)) {
                    exterminador[j].sonido(exterminador[j].getAgarrarItem());
                    exterminador[j].curar();
                    elixirDeVida.setEstadoElixir(false);
                }

                // BUSCA LA COLISION CON LOS ELIXIR DE VIDA
                if (exterminador[j].chocasteCon(zapatilla)) {
                    exterminador[j].sonido(exterminador[j].getAgarrarItem());
                    exterminador[j].aumentarVelocidad();
                    zapatilla.setEstadoZapatilla(false);
                }

                // BUSCA LA COLISION CONTRA ALGUN EDIFICIO
                if (exterminador[j].cualChocaste(edificio) != -1) {
                    exterminador[j].moverEnColision(edificio);// EVITA QUE EL PERSONAJE SIGA AVANZANDO CUANDO CHOQUE UNA
                    // PARED
                }

                exterminador[j].moverSinSalirDePantalla(entorno);

                if (exterminador[j].chocasteCon(serpiente)) {
                    exterminador[j].setVida(exterminador[j].getVida() - serpiente.getDanio());
                }

                // BUSCA LA COLISION CONTRA ALGUN ENEMIGO
                if (exterminador[j].cualChocaste(enemigo) != -1) {
                    exterminador[j].setVida(
                            exterminador[j].getVida() - enemigo[exterminador[j].cualChocaste(enemigo)].getDanio());
                }

                if (exterminador[j].isDispareArma()) {
                    for (int nProy = 0; nProy < armasEnJuego.length; nProy++) {
                        if (armasEnJuego[nProy] == null && exterminador[j].isDispareArma()) {
                            armasEnJuego[nProy] = exterminador[j].dispararArma();
                        }
                    }
                }
                // mata al exterminador
                if (exterminador[j].getVida() <= 0) {
                    int posLibre = -1;
                    for (int i = 0; i < armasEnJuego.length; i++) {
                        if (armasEnJuego[i] == null) {
                            posLibre = i;
                            break;
                        }
                    }
                    armasEnJuego[posLibre] = exterminador[j].kamikaze();
                    exterminador[j] = null;
                }
            }
        }

//*************************************************************************************
        // SECCION PROYECTILES CON CLASES APARTE
        for (int nProy = 0; nProy < armasEnJuego.length; nProy++) {
            if (armasEnJuego[nProy] != null) {
                armasEnJuego[nProy].dibujar(entorno);
                armasEnJuego[nProy].mover();
                armasEnJuego[nProy] = armasEnJuego[nProy].estadosDelArma(entorno, edificio, enemigo, armasEnJuego,
                        exterminador, serpiente);
            }
        }
//*************************************************************************************

        // COMPORTAMIENTO DE LOS ENEMIGOS
        for (int en = 0; en < enemigo.length; en++) {// en = numero de enemigo
            if (serpiente != null) {
                enemigo[en] = null;
            }
            if (enemigo[en] != null) {
                // SI EL ENEMIGO CHOCA CONTRA UNA PARED o UN ENEMIGO, y EL ENEMIGO NO ES NULL
                if (enemigo[en].cualChocaste(edificio) != -1
                        || (enemigo[en].cualChocaste(enemigo) != -1 && enemigo[en].cualChocaste(enemigo) != en)) {

                    enemigo[en].buscarJugador(exterminador);
                    enemigo[en].moverEnColision(enemigo[enemigo[en].cualChocaste(enemigo)]);

                    if (enemigo[en].cualChocaste(edificio) != -1) {// CHOCA CONTRA UNA PARED
                        enemigo[en].moverRodeando(edificio, exterminador);
                        // HACE QUE EL ENEMIGO RODEE LA PARED POR EL CAMINO MAS CORTO
                    }
                } else {
                    enemigo[en].mover();
                    enemigo[en].buscarJugador(exterminador);
                }

                // ENEMIGO COMPORTAMIENTO CON ARRAY ARMA
                int posLibre = -1;
                for (int nProy = 0; nProy < armasEnJuego.length; nProy++) {

                    // BUSCA UNA POSICION LIBRE EN ARMA PARA DISPARAR
                    if (armasEnJuego[nProy] == null) {
                        posLibre = nProy;
                        break;
                    }

                }
                if (posLibre != -1) {
                    armasEnJuego[posLibre] = enemigo[en].dispararArma();
                }

                // mata al enemigo
                if (enemigo[en].getVida() <= 0) {
                    enemigo[en] = null;
                    puntaje++; // AUMENTA EL PUNTAJE CUANDO MUERE UN ENEMIGO
                    factorSpawneo--; // AUMENTA LA VELOCIDAD DE APARICIONES
                }

                if (enemigo[en] != null) {
                    enemigo[en].dibujar(entorno);
                }
            }
        }

        if (puntaje % 10 == 0 && puntaje != 0) {
            if (serpiente == null) {
                serpiente = new Serpiente(entorno.ancho() / 2, entorno.alto() / 2, 20, 0, 1, puntaje / 2);
            }

            if (!serpiente.estadoSerpiente()) {
                serpiente = null;
                puntaje += 1;
             // NUEVO NIVEL
                nivel = new GeneradorDeNiveles();
                edificio = new Edificio[nivel.getEdificios().length];
                edificio = nivel.getEdificios();

                for (int i = 0; i < exterminador.length; i++) {
                    if (exterminador[i] != null) {
                        exterminador[i].setX(exterminador[i].getxAnterior());
                        exterminador[i].setY(exterminador[i].getyAnterior());
                    }
                }
                
                for (int i = 0; i < exterminador.length; i++) {
                    if (exterminador[i] != null) {
                        if (exterminador[i].getArmasDisponibles() < exterminador[i].getArmasJugador().length - 1) {
                            armasDisponiblesParaJugadores++;
                            anuncio = puntaje;
                            break;
                        }
                    }
                }
                for (int i = 0; i < exterminador.length; i++) {
                    if (exterminador[i] != null) {
                        exterminador[i].setArmasDisponibles(armasDisponiblesParaJugadores);
                    }

                }

            }
            if (serpiente != null) {
                if (serpiente.cualChocaste(edificio) != -1) {
                    serpiente.buscarJugador(exterminador);
                    // serpiente.moverEnColision(enemigo[serpiente.cualChocaste(enemigo)]);

                    if (serpiente.cualChocaste(edificio) != -1) {// CHOCA CONTRA UNA PARED
                        serpiente.moverRodeando(edificio, exterminador);
                        // HACE QUE EL ENEMIGO RODEE LA PARED POR EL CAMINO MAS CORTO
                    }
                } else {
                    serpiente.mover();
                    serpiente.buscarJugador(exterminador);
                }
            }
            if (serpiente != null) {
                serpiente.dibujar(entorno);
            }
        }

        // EJECUTAR CONTROLES
        for (int nExterminador = 0; nExterminador < exterminador.length; nExterminador++) {
            for (int nSprite = 0; nSprite < spriteExterminador[nExterminador].length; nSprite++) {
                if (exterminador[nExterminador] != null) {
                    // DIBUJA CUANDO ESTA PARADO
                    exterminador[nExterminador].dibujar(entorno, spriteExterminador[nExterminador][1]);
                }
            }
            if (controlJugador[nExterminador] != null) {
                // DIBUJA CUANDO ESTA EN MOVIMIENTO
                controlJugador[nExterminador].ejecutarControl(entorno, exterminador[nExterminador],
                        spriteExterminador[nExterminador]);
            }
        }

        // CADA X CANTIDAD DE TIEMPO SE INVOCAN CAJAS DE MUNICIONES
        cajaDeMuniciones = cajaDeMuniciones.invocarCajaMuniciones(entorno);
        cajaDeMuniciones.dibujarCajaDeMunicion(entorno);

        // CADA X CANTIDAD DE TIEMPO SE INVOCAN ELIXIRS
        elixirDeVida = elixirDeVida.invocarElixir(entorno);
        elixirDeVida.dibujarElixirDeVida(entorno);

        // CADA X CANTIDAD DE TIEMPO SE INVOCAN ELIXIRS
        zapatilla = zapatilla.invocarZapatilla(entorno);
        zapatilla.dibujarZapatilla(entorno);

        // DIBUJA LAS PAREDES DEL CONTORNO
        nivel.dibujarParedesContorno(entorno);

        // DIBUJA LA INTERFAZ
        interfaz.dibujarMunicionDe(exterminador, entorno);
        interfaz.dibujarPuntaje(puntaje, entorno);
        interfaz.dibujarVida(exterminador, entorno);
        interfaz.gameOver(exterminador, entorno);
        
        if(interfaz.isReiniciar()){
            sonidoBackground.stop();
            interfaz.setReiniciar(false);
            reiniciarJuego();
        }

        for (int i = 0; i < exterminador.length; i++) {
            if (exterminador[i] != null) {
                if (anuncio == puntaje) {
                    interfaz.anuncioDeNuevaArma(exterminador[i].getArmasJugador()[armasDisponiblesParaJugadores],
                            entorno);
                }
                break;
            }
        }
    }

    public void tick() {
        if (!gameStart) {
            pantallaDeTitulos();
        } else {
            try {
                mainGame();
            } catch (LineUnavailableException e) {
            }
        }
    }

    @SuppressWarnings("unused")
    public static void main(String[] args) {
        Juego juego = new Juego();
    }
}
