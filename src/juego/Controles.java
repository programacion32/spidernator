/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

import entorno.Entorno;
import java.awt.Image;

import javax.sound.sampled.LineUnavailableException;

/**
 *
 * @author Matt
 */
public class Controles {

    private char avanzar;
    private char retroceder;
    private char girarDerecha;
    private char girarIzquierda;
    private char dispararArma;
    private char cambiarArmaAdelante;
    private char cambiarArmaAtras;
    private char movEspecial;

    Controles(char avanzar, char retroceder, char girarDerecha, char girarIzquierda,
            char dispararArma, char cambiarArmaAdelante, char cambiarArmaAtras) {
        this.avanzar = avanzar;
        this.retroceder = retroceder;
        this.girarDerecha = girarDerecha;
        this.girarIzquierda = girarIzquierda;
        this.dispararArma = dispararArma;
        this.cambiarArmaAdelante = cambiarArmaAdelante;
        this.cambiarArmaAtras = cambiarArmaAtras;
    }

    public void ejecutarControl(Entorno entorno, Exterminador exterminador, Image[] sprite) {
        if (exterminador != null) {
            if (entorno.estaPresionada(avanzar)) {
                exterminador.moverAdelante();

        		if (!exterminador.getCaminar().isActive()) {

        			try {
        				exterminador.sonido(exterminador.getCaminar());
        			} catch (LineUnavailableException e) {
        			}
        		}
                exterminador.dibujar(entorno, sprite[exterminador.getSpriteActual()]);
            }

            if (entorno.estaPresionada(retroceder)) {
                exterminador.moverAtras();
            }
            if (entorno.estaPresionada(girarIzquierda)) {
                exterminador.moverIzquierda();
            }
            if (entorno.estaPresionada(girarDerecha)) {
                exterminador.moverDerecha();
            }


            if (entorno.sePresiono(dispararArma)) {
                exterminador.setDispareArma(true);//si dispara pone el set del jugador en true
            }

            // CAMBIAR ARMAS
            if (entorno.sePresiono(cambiarArmaAdelante)) {
                exterminador.cambiarArmaAdelante();
            }
            if (entorno.sePresiono(cambiarArmaAtras)) {
                exterminador.cambiarArmaAtras();
            }

        }
    }

}
