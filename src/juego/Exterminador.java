package juego;

import java.awt.Color;
import java.awt.Image;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

import Armas.Arma;
import Enemigo.Enemigo;
import Enemigo.Serpiente;
import Items.CajaDeMuniciones;
import Items.ElixirDeVida;
import Items.Zapatilla;
import Nivel.Edificio;
import entorno.Entorno;

public class Exterminador {

    private double x;
    private double y;
    private int base;
    private int altura;
    private double angulo;

    private double velocidad;
    private double velocidadMaxima;

    private double movimientoReducido;// porcentaje con el cual determina la velocidad cuando camina de espalda

    private double yAnterior;
    private double xAnterior;

    private long tiempoSpriteAnterior;
    private int spriteActual;
    private int cantidadSprites;

    // ARRAY DE ARMAS 0-Pistola 1-Escopeta 2-Mina 3-Domo
    private int armasDisponibles;
    private String[] armasJugador = {"Pistola", "Escopeta", "Mina", "Domo"};
    private Arma armas;

    private String armaActual;

    private boolean dispareArma;

    private int vida;
    private int vidaMaxima;

    // nuevo
    private int municionDisponiblePistola;
    private int municionDisponibleEscopeta;
    private int municionDisponibleMina;
    private int municionDisponibleDomo;

    private long disparoAnteriorPistola;
    private long disparoAnteriorEscopeta;

    private long disparoAnteriorMina;

    private double tiempoEntreDisparoPistola;
    private double tiempoEntreDisparoEscopeta;
    private double tiempoEntreDisparoMina;

    private Clip agarrarItem;
    private Clip caminar;

    private Clip dispararPistola;
    private Clip dispararEscopeta;

    public Exterminador(double x, double y, int base, int altura, double angulo, double velocidad, int vida) {
        this.x = x;
        this.y = y;
        this.base = base;
        this.altura = altura;
        this.angulo = angulo;
        this.velocidad = velocidad;
        this.velocidadMaxima = velocidad;
        this.movimientoReducido = 0.5;

        this.tiempoSpriteAnterior = 0;
        this.cantidadSprites = 4;
        this.spriteActual = 0;

        this.armaActual = armasJugador[0];

        this.vida = vida;
        this.vidaMaxima = vida;

        this.armasDisponibles = 0;

        // nuevo
        this.municionDisponiblePistola = 20;
        this.municionDisponibleEscopeta = 20;
        this.municionDisponibleMina = 10;
        this.municionDisponibleDomo = 2;

        this.tiempoEntreDisparoPistola = 2;
        this.tiempoEntreDisparoEscopeta = 5;
        this.tiempoEntreDisparoMina = 1;

        this.agarrarItem = entorno.Herramientas.cargarSonido("itemObtenido.wav");
        this.caminar = entorno.Herramientas.cargarSonido("caminar.wav");

        this.dispararEscopeta = entorno.Herramientas.cargarSonido("escopeta.wav");
        this.dispararPistola = entorno.Herramientas.cargarSonido("pistola.wav");
        
        this.xAnterior = x;
        this.yAnterior = y;
    }

    public void sonido(Clip sonido) throws LineUnavailableException {
        sonido.setFramePosition(0);
        sonido.start();
    }

    public void moverSinSalirDePantalla(Entorno entorno) {
        if (x <= 80) { // Choca Izquierda
            x = 80;
        }
        if (x > entorno.ancho() - 80) {
            x = entorno.ancho() - 80;
        }
        if (y < 80) {
            y = 80;
        }
        if (y > entorno.alto() - 40) {
            y = entorno.alto() - 40;
        }
    }

    public void moverEnColision(Edificio[] edificio) {
        int factorDesplazamiento = 4;
        for (int i = 0; i < edificio.length; i++) {
            if (Exterminador.this.chocasteCon(edificio[i])) {
                if (x + (base / 2) - factorDesplazamiento <= (edificio[i].getX() - (edificio[i].getAncho() / 2))) { // Choca
                    // Izquierda
                    x = (edificio[i].getX() - edificio[i].getAncho() / 2) - (base / 2);
                } else if (x - (base / 2)
                        + factorDesplazamiento >= (edificio[i].getX() + (edificio[i].getAncho() / 2))) { // Choca
                    // Derecha
                    x = (edificio[i].getX() + edificio[i].getAncho() / 2) + (base / 2);
                } else if (y - (altura / 2)
                        + factorDesplazamiento >= (edificio[i].getY() + (edificio[i].getAlto() / 2))) { // Choca Abajo
                    y = (edificio[i].getY() + edificio[i].getAlto() / 2) + (base / 2);
                } else if (y + (altura / 2)
                        - factorDesplazamiento <= (edificio[i].getY() - (edificio[i].getAlto() / 2))) { // Choca
                    // Arriba
                    y = (edificio[i].getY() - edificio[i].getAlto() / 2) - (base / 2);

                }
            }
        }
    }

    // DEVUELVE EL NUMERO DE EDIFICIO CONTRA EL QUE COLISIONO
    public int cualChocaste(Edificio[] edificio) {
        for (int nEd = 0; nEd < edificio.length; nEd++) {
            if (this.chocasteCon(edificio[nEd])) {
                return nEd;
            }
        }
        return -1;
    }

    private boolean chocasteCon(Edificio edificio) {
        if (edificio != null) {
            return (x + altura / 2 >= edificio.getX() - edificio.getAncho() / 2
                    && x - altura / 2 <= edificio.getX() + edificio.getAncho() / 2)
                    && (y + altura / 2 >= edificio.getY() - edificio.getAlto() / 2
                    && y - altura / 2 <= edificio.getY() + edificio.getAlto() / 2);
        }
        return false;
    }

    // DEVUELVE EL NUMERO DE ENEMIGO CONTRA EL QUE COLISIONO
    public int cualChocaste(Enemigo[] enemigo) {
        for (int nEn = 0; nEn < enemigo.length; nEn++) {
            if (this.chocasteCon(enemigo[nEn])) {
                return nEn;
            }
        }
        return -1;
    }

    private boolean chocasteCon(Enemigo enemigo) {
        if (enemigo != null) {
            return (x + altura / 2 >= enemigo.getX() - enemigo.getRadio()
                    && x - altura / 2 <= enemigo.getX() + enemigo.getRadio())
                    && (y + altura / 2 >= enemigo.getY() - enemigo.getRadio()
                    && y - altura / 2 <= enemigo.getY() + enemigo.getRadio());
        }
        return false;
    }

    // ----------- CHOCO CONTRA UN ITEM DE MUNICIONES
    public boolean chocasteCon(CajaDeMuniciones municion) {
        if (municion.isEstadoCaja()) {// COMPRUEBA QUE SEA CONSUMIBLE
            return (x + altura / 2 >= municion.getX() - municion.getRadio()
                    && x - altura / 2 <= municion.getX() + municion.getRadio())
                    && (y + altura / 2 >= municion.getY() - municion.getRadio()
                    && y - altura / 2 <= municion.getY() + municion.getRadio());
        }
        return false;
    }

    // ----------- CHOCO CONTRA UN ITEM DE ELIXIR DE VIDA
    public boolean chocasteCon(ElixirDeVida elixirVida) {
        if (elixirVida.isEstadoElixir()) {// comprueba que el elixir sea utilizable

            return (x + altura / 2 >= elixirVida.getX() - elixirVida.getRadio()
                    && x - altura / 2 <= elixirVida.getX() + elixirVida.getRadio())
                    && (y + altura / 2 >= elixirVida.getY() - elixirVida.getRadio()
                    && y - altura / 2 <= elixirVida.getY() + elixirVida.getRadio());
        }
        return false;
    }

    // ----------- CHOCO CONTRA UN ITEM DE ELIXIR DE VIDA
    public boolean chocasteCon(Zapatilla zapatilla) {
        if (zapatilla.isEstadoZapatilla()) {// comprueba que el elixir sea utilizable

            return (x + altura / 2 >= zapatilla.getX() - zapatilla.getRadio()
                    && x - altura / 2 <= zapatilla.getX() + zapatilla.getRadio())
                    && (y + altura / 2 >= zapatilla.getY() - zapatilla.getRadio()
                    && y - altura / 2 <= zapatilla.getY() + zapatilla.getRadio());
        }
        return false;
    }

    // -----------SI CHOCO CONTRA UN ITEM DE MUNICION
    public void recargarBalas() {
        this.municionDisponiblePistola = 20;
        this.municionDisponibleEscopeta = 20;
        this.municionDisponibleMina = 10;
        this.municionDisponibleDomo = 2;
    }

    // -----------SI CHOCO CONTRA UN ITEM DE ELIXIR DE VIDA
    public void curar() {
        this.vida += this.vidaMaxima / 2;
        if (this.vida > this.vidaMaxima) {
            this.vida = this.vidaMaxima;
        }
    }

    // -----------SI CHOCO CONTRA UN ITEM DE ZAPATILLA
    public void aumentarVelocidad() {
        if (velocidad >= velocidadMaxima) {
            this.velocidad = velocidadMaxima * 1.5;
        } else {
            this.velocidad = this.velocidadMaxima;
        }
    }

    // -----------SI CHOCA CONTRA ALGUN ELEMENTO QUE INFLIJA DAÑO
    public void quitarVida(double danio) {
        vida -= danio;
    }

    // ----------- AL MORIR EL EXTERMINADOR EXPLOTA
    public Arma kamikaze() {
        Arma kamikaze = new Arma();
        kamikaze.setExplosion(x, y);
        return kamikaze;
    }

    // DIBUJA AL EXTERMINADOR
    public void dibujar(Entorno entorno, Image sprite) {

        entorno.dibujarRectangulo(Math.cos(angulo + (5 * Math.PI / 180)) * 100 + x, Math.sin(angulo + (5 * Math.PI / 180)) * 100 + y, 1, 150, angulo + Math.PI / 2, Color.RED);
        if (System.currentTimeMillis() / 100 > tiempoSpriteAnterior + 1) {
            if (this.spriteActual < this.cantidadSprites - 1) {
                this.spriteActual++;
            } else {
                spriteActual = 0;
            }
            tiempoSpriteAnterior = System.currentTimeMillis() / 100;
        }

        entorno.dibujarImagen(sprite, Math.cos(angulo) * 4 + x, Math.sin(angulo) * 4 + y, angulo + Math.PI / 2, 0.5);

    }

    public boolean chocasteCon(Serpiente serpiente) {
        if (serpiente != null) {
            for (int i = serpiente.getNodoCola(); i < serpiente.getNodosSerpiente().length; i++) {
                if (serpiente.getNodosSerpiente()[i] != null) {
                    if ((x + base / 2 >= serpiente.getNodosSerpiente()[i].getX()
                            - serpiente.getNodosSerpiente()[i].getRadio()
                            && x - base / 2 <= serpiente.getNodosSerpiente()[i].getX()
                            + serpiente.getNodosSerpiente()[i].getRadio())
                            && (y + altura / 2 >= serpiente.getNodosSerpiente()[i].getY()
                            - serpiente.getNodosSerpiente()[i].getRadio()
                            && y - altura / 2 <= serpiente.getNodosSerpiente()[i].getY()
                            + serpiente.getNodosSerpiente()[i].getRadio())) {
                        serpiente.setGolpeado(false);
                        this.vida -= serpiente.getDanio();
                        return true;
                    }
                }
            }

        }
        return false;
    }

    public Arma dispararArma() {
        armas = new Arma();
        boolean noHayDisparos = true;
        if ((this.armaActual.equals("Pistola") && (this.municionDisponiblePistola > 0)
                && (System.currentTimeMillis() / 100 > this.disparoAnteriorPistola + tiempoEntreDisparoPistola))) {
            try {
                this.sonido(dispararPistola);
            } catch (LineUnavailableException e) {
            }
            armas.setPistola(x, y, angulo);
            this.municionDisponiblePistola--;
            this.disparoAnteriorPistola = System.currentTimeMillis() / 100;
            noHayDisparos = false;
        }
        if ((this.armaActual.equals("Escopeta") && (this.municionDisponibleEscopeta > 0)
                && (System.currentTimeMillis() / 100 > this.disparoAnteriorEscopeta + tiempoEntreDisparoEscopeta))) {
            try {
                sonido(dispararEscopeta);
            } catch (LineUnavailableException e) {
            }
            armas.setEscopeta(x, y, angulo);
            this.municionDisponibleEscopeta--;
            this.disparoAnteriorEscopeta = System.currentTimeMillis() / 100;
            noHayDisparos = false;
        }
        if ((this.armaActual.equals("Mina") && (this.municionDisponibleMina > 0)
                && (System.currentTimeMillis() / 100 > this.disparoAnteriorMina + tiempoEntreDisparoMina))) {
            armas.setMina(x, y, angulo);
            this.municionDisponibleMina--;
            this.disparoAnteriorMina = System.currentTimeMillis() / 100;
            noHayDisparos = false;
        }
        if ((this.armaActual.equals("Domo") && (this.municionDisponibleDomo > 0))) {
            armas.setDomo(x, y);
            this.municionDisponibleDomo--;
            this.disparoAnteriorMina = System.currentTimeMillis() / 100;
            noHayDisparos = false;
        }

        dispareArma = false;
        System.out.println("Dispare - " + this.armaActual);

        if (noHayDisparos) {
            return null;
        } else {
            return armas;
        }
    }

    // INTERCAMBIA LAS ARMAS
    public void cambiarArmaAdelante() {// 0 pistola, 1 escopeta, 2 mina, 3 domo
        if (this.armaActual.equals(this.armasJugador[this.armasDisponibles])) {
            this.armaActual = this.armasJugador[0];
            return;
        } else {
            for (int i = 0; i < this.armasDisponibles; i++) {
                if (this.armaActual.equals(this.armasJugador[i])) {
                    this.armaActual = this.armasJugador[i + 1];
                    return;
                }
            }
        }
    }

    public void cambiarArmaAtras() {// 0 pistola, 1 escopeta, 2 mina, 3 domo
        if (this.armaActual.equals(this.armasJugador[0])) {
            this.armaActual = this.armasJugador[this.armasDisponibles];
            return;
        } else {
            for (int i = this.armasDisponibles; i >= 0; i--) {
                if (this.armaActual.equals(this.armasJugador[i])) {
                    this.armaActual = this.armasJugador[i - 1];
                    return;
                }
            }
        }
    }

    // __ __ _____ _____ __ __ ___ _____ _ _ _____ ___
    // | \/ |/ _ \ \ / /_ _| \/ |_ _| ____| \ | |_ _/ _ \
    // | |\/| | | | \ \ / / | || |\/| || || _| | \| | | || | | |
    // | | | | |_| |\ V / | || | | || || |___| |\ | | || |_| |
    // |_| |_|\___/ \_/ |___|_| |_|___|_____|_| \_| |_| \___/
    public void moverAdelante() {
        x += velocidad * Math.cos(angulo);
        y += velocidad * Math.sin(angulo);
    }

    public void moverAtras() {
        x -= velocidad * movimientoReducido * Math.cos(angulo);
        y -= velocidad * movimientoReducido * Math.sin(angulo);
    }

    public void moverIzquierda() {
        angulo -= 0.08;
    }

    public void moverDerecha() {
        angulo += 0.08;
    }

    // ___ ___ _____ _____ ___ ___ ___ _ _ _ ___ ___ ___ _____ _____ ___ ___ ___
    // / __| __|_ _|_ _| __| _ \/ __| /_\ | \| | \ / __| __|_ _|_ _| __| _ \/
    // __|
    // | (_ | _| | | | | | _|| /\__ \ / _ \| .` | |) | \__ \ _| | | | | | _||
    // /\__ \
    // \___|___| |_| |_| |___|_|_\|___/ /_/ \_\_|\_|___/ |___/___| |_| |_|
    // |___|_|_\|___/
    public String getArmaActual() {
        return armaActual;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getAngulo() {
        return angulo;
    }

    public void setAngulo(double angulo) {
        this.angulo = angulo;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public double getxAnterior() {
        return xAnterior;
    }

    public void setxAnterior(double xAnterior) {
        this.xAnterior = xAnterior;
    }

    public double getyAnterior() {
        return yAnterior;
    }

    public void setyAnterior(double yAnterior) {
        this.yAnterior = yAnterior;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public double getMovimientoReducido() {
        return movimientoReducido;
    }

    public void setMovimientoReducido(double movimientoReducido) {
        this.movimientoReducido = movimientoReducido;
    }

    public int getSpriteActual() {
        return spriteActual;
    }

    public void setSpriteActual(int spriteActual) {
        this.spriteActual = spriteActual;
    }

    public String[] getArmasJugador() {
        return armasJugador;
    }

    public void setArmasJugador(String[] armasJugador) {
        this.armasJugador = armasJugador;
    }

    public void setArmaActual(String armaActual) {
        this.armaActual = armaActual;
    }

    public int getVida() {
        return vida;
    }

    public void setVida(double d) {
        this.vida = (int) d;
    }

    public int getArmasDisponibles() {
        return armasDisponibles;
    }

    public void setArmasDisponibles(int armasDisponibles) {
        this.armasDisponibles = armasDisponibles;
    }

    public boolean isDispareArma() {
        return dispareArma;
    }

    public void setDispareArma(boolean dispareArma) {
        this.dispareArma = dispareArma;
    }

    public int getMunicionDisponiblePistola() {
        return municionDisponiblePistola;
    }

    public void setMunicionDisponiblePistola(int municionDisponiblePistola) {
        this.municionDisponiblePistola = municionDisponiblePistola;
    }

    public int getMunicionDisponibleEscopeta() {
        return municionDisponibleEscopeta;
    }

    public void setMunicionDisponibleEscopeta(int municionDisponibleEscopeta) {
        this.municionDisponibleEscopeta = municionDisponibleEscopeta;
    }

    public int getMunicionDisponibleMina() {
        return municionDisponibleMina;
    }

    public void setMunicionDisponibleMina(int municionDisponibleMina) {
        this.municionDisponibleMina = municionDisponibleMina;
    }

    public double getVelocidadMaxima() {
        return velocidadMaxima;
    }

    public void setVelocidadMaxima(double velocidadMaxima) {
        this.velocidadMaxima = velocidadMaxima;
    }

    public int getVidaMaxima() {
        return vidaMaxima;
    }

    public void setVidaMaxima(int vidaMaxima) {
        this.vidaMaxima = vidaMaxima;
    }

    public Clip getAgarrarItem() {
        return agarrarItem;
    }

    public void setAgarrarItem(Clip agarrarItem) {
        this.agarrarItem = agarrarItem;
    }

    public int getMunicionDisponibleDomo() {
        return municionDisponibleDomo;
    }

    public void setMunicionDisponibleDomo(int municionDisponibleDomo) {
        this.municionDisponibleDomo = municionDisponibleDomo;
    }

    public Clip getCaminar() {
        return caminar;
    }

    public void setCaminar(Clip caminar) {
        this.caminar = caminar;
    }

}
