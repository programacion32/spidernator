package juego;

import java.awt.Color;
import entorno.Entorno;

public class Jugador {
	private double x;
	private double y;
	private int base;
	private int altura;
	private double angulo;
	private double velocidad;
	private double movimientoReducido;
	private Color color;
	private boolean sentido;
	
	private double yAnterior;
	private double xAnterior;
	
	public Jugador(double x, double y, int base, int altura, double angulo,
			double velocidad, Color color) {
		super();
		this.x = x;
		this.y = y;
		this.base = base;
		this.altura = altura;
		this.angulo = angulo;
		this.velocidad = velocidad;
		this.color = color;
		this.movimientoReducido = 0.5;
		this.sentido = true;
		this.xAnterior=x;
		this.yAnterior=y;
	}

	public boolean chocasteCon(Edificio f){
		return (x+altura>= f.getX()-f.getAncho()/2 && x-altura<= f.getX()+f.getAncho()/2) && (y+altura>= f.getY()-f.getAlto()/2 && y-altura<= f.getY()+f.getAlto()/2);
}
	
	
	
	
	
	
//	  
//	  |  \/  |/ _ \ \   / /_ _|  \/  |_ _| ____| \ | |_   _/ _ \ 
//	  | |\/| | | | \ \ / / | || |\/| || ||  _| |  \| | | || | | |
//	  | |  | | |_| |\ V /  | || |  | || || |___| |\  | | || |_| |
//	  |_|  |_|\___/  \_/  |___|_|  |_|___|_____|_| \_| |_| \___/ 
	                                                            
	public void MoverAdelante() {
		x += velocidad * Math.cos(angulo);
		y += velocidad * Math.sin(angulo);
		this.sentido = true;

	}

	public void MoverAtras() {
		x -= velocidad * movimientoReducido * Math.cos(angulo);
		y -= velocidad * movimientoReducido * Math.sin(angulo);
		this.sentido = false;
	}

	public void MoverIzquierda() {
		if (sentido)
			angulo -= 0.08;
		else
			angulo += 0.08;
	}

	public void MoverDerecha() {
		if (sentido)
			angulo += 0.08;
		else
			angulo -= 0.08;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getAngulo() {
		return angulo;
	}

	public void setAngulo(double angulo) {
		this.angulo = angulo;
	}

	public double getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(double velocidad) {
		this.velocidad = velocidad;
	}

	public double getxAnterior() {
		return xAnterior;
	}

	public void setxAnterior(double xAnterior) {
		this.xAnterior = xAnterior;
	}

	public double getyAnterior() {
		return yAnterior;
	}

	public void setyAnterior(double yAnterior) {
		this.yAnterior = yAnterior;
	}

	

	public void dibujar(Entorno e) {
		e.dibujarTriangulo(x, y, altura, base, angulo, color);
	}
}
