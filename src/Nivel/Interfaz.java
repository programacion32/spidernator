package Nivel;

import java.awt.Color;

import entorno.Entorno;
import entorno.Herramientas;
import juego.Exterminador;

import java.awt.Image;

public class Interfaz {

    private int infoJugadorX;
    private int infoJugadorY;
    private int movX;
    private Image iconoDeBala;
    private Image gameOver;
    private Image fondoNegro;
    private boolean reiniciar;

    public Interfaz() {
        this.infoJugadorX = 750;
        this.infoJugadorY = 500;
        this.movX = 100;
        this.iconoDeBala = Herramientas.cargarImagen("bala.png");
        this.gameOver = Herramientas.cargarImagen("GameOver.png");
        this.fondoNegro = Herramientas.cargarImagen("fondoNegro.png");
    }

    public void dibujarMunicionDe(Exterminador[] j, Entorno e) {

        for (int i = 0; i < j.length; i++) {
            if (j[i] != null) {
                e.cambiarFont("arial", 30, Color.WHITE);
                e.dibujarImagen(iconoDeBala, infoJugadorX * i + movX, infoJugadorY + 50, 0, 0.2);
                e.escribirTexto(j[i].getArmaActual(), infoJugadorX * i + movX, infoJugadorY + 10);

                if (j[i].getArmaActual().equals("Pistola")) {
                    e.escribirTexto("" + j[i].getMunicionDisponiblePistola(), infoJugadorX * i + movX + 30, infoJugadorY + 63);
                }

                if (j[i].getArmaActual().equals("Escopeta")) {
                    e.escribirTexto("" + j[i].getMunicionDisponibleEscopeta(), infoJugadorX * i + movX + 30, infoJugadorY + 63);
                }

                if (j[i].getArmaActual().equals("Mina")) {
                    e.escribirTexto("" + j[i].getMunicionDisponibleMina(), infoJugadorX * i + movX + 30, infoJugadorY + 63);
                }
                if (j[i].getArmaActual().equals("Domo")) {
                    e.escribirTexto("" + j[i].getMunicionDisponibleDomo(), infoJugadorX * i + movX + 30, infoJugadorY + 63);
                }
            } else {

                e.cambiarFont("arial", 40, Color.WHITE);
                e.escribirTexto("Muerto", infoJugadorX * i + 60, infoJugadorY + 40);
            }

        }
    }

    public void dibujarPuntaje(int puntaje, Entorno e) {
        String p = "" + puntaje;
        e.cambiarFont("arial", 70, Color.WHITE);
        e.escribirTexto("" + puntaje, (e.ancho() / 2 - p.length() * 10), 60);
    }

    public void dibujarVida(Exterminador[] j, Entorno e) {
        e.cambiarFont("arial", 20, Color.WHITE);
        for (int i = 0; i < j.length; i++) {
            if (j[i] != null) {
                e.escribirTexto("" + j[i].getVida(), j[i].getX() - j[i].getBase() / 2, j[i].getY() - j[i].getAltura());
            }
        }

    }

    public void gameOver(Exterminador[] exterminador, Entorno e) {

        boolean estanTodosVivos = false;
        for (int j = 0; j < exterminador.length; j++) {
            if (exterminador[j] != null) {
                estanTodosVivos = true;
            }
        }
        if (!estanTodosVivos) {
            e.dibujarImagen(fondoNegro, e.ancho() / 2, e.alto() / 2, 0);
            e.dibujarImagen(gameOver, e.ancho() / 2, e.alto() / 3 + 50, 0, 1);
            e.cambiarFont("console", 40, Color.yellow);
            e.escribirTexto("  -  ENTER  - ", e.ancho() / 3 + 60, e.alto() * 3 / 4);
            e.escribirTexto("para continuar", e.ancho() / 3 + 60, e.alto() * 3 / 4 + 50);
            if(e.sePresiono(e.TECLA_ENTER)){
                this.reiniciar = true;
            }
        }
    }
    
    

    public void anuncioDeNuevaArma(String arma, Entorno e) {
        e.cambiarFont("arial", 40, Color.RED);
        e.escribirTexto("Nueva arma: " + arma, (e.ancho() / 2 - arma.length() * 20), 100);
    }

    public boolean isReiniciar() {
        return reiniciar;
    }

    public void setReiniciar(boolean reiniciar) {
        this.reiniciar = reiniciar;
    }
    
    
}
