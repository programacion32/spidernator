package Nivel;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Edificio {

    private double x;
    private double y;
    private double ancho;
    private double alto;

    public Edificio(double x, double y, int numOpcion) {

        this.x = x;
        this.y = y;

        switch (numOpcion) {
            case 1:
                this.ancho = 96;
                this.alto = 96;
                break;

            case 2:
                this.ancho = 96;
                this.alto = 32;
                break;

            case 3:
                this.ancho = 64;
                this.alto = 96;
                break;

            case 4:
                this.ancho = 96;
                this.alto = 64;
                break;
            case 5:

                this.ancho = 64;
                this.alto = 32;
                break;
        }
    }

//   ___ ___ _____ _____ ___ ___  ___     _   _  _ ___    ___ ___ _____ _____ ___ ___  ___ 
//  / __| __|_   _|_   _| __| _ \/ __|   /_\ | \| |   \  / __| __|_   _|_   _| __| _ \/ __|
// | (_ | _|  | |   | | | _||   /\__ \  / _ \| .` | |) | \__ \ _|  | |   | | | _||   /\__ \
//  \___|___| |_|   |_| |___|_|_\|___/ /_/ \_\_|\_|___/  |___/___| |_|   |_| |___|_|_\|___/   
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getAncho() {
        return ancho;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }

    public double getAlto() {
        return alto;
    }

    public void setAlto(double alto) {
        this.alto = alto;
    }

}
