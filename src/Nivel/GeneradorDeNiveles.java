package Nivel;

import java.awt.Image;
import java.awt.Point;

import entorno.Entorno;
import entorno.Herramientas;

import java.util.Random;

public class GeneradorDeNiveles {

    private Image ladrillosEdificio;
    private Image suelo;
    private Image paredesContorno;
    private Point[] puntosValidos = new Point[14];
    private Edificio[] edificios;

    public GeneradorDeNiveles() {
        shuffleImg();
        invocarEdificios();
    }

    private void shuffleImg() {
        String[] nomSuelo = {"Suelo01.png", "Suelo02.png"};
        String[] nomParedContorno = {"Pared01.png", "Pared02.png"};
        String[] nomLadrilloEdif = {"bloque01 32x32.png", "bloque02 32x32.png"};
        Random rand = new Random();
        this.ladrillosEdificio = Herramientas.cargarImagen(nomLadrilloEdif[rand.nextInt(nomLadrilloEdif.length)]);
        this.suelo = Herramientas.cargarImagen(nomSuelo[rand.nextInt(nomSuelo.length)]);
        this.paredesContorno = Herramientas.cargarImagen(nomParedContorno[rand.nextInt(nomParedContorno.length)]);
    }

    private void shufflePuntosValidos() {
        Random rand = new Random();
        Point puntoAnterior;
        int numRand = 0;
        for (int i = 0; i < this.puntosValidos.length; i++) {
            numRand = rand.nextInt(this.puntosValidos.length);
            puntoAnterior = this.puntosValidos[numRand];
            this.puntosValidos[numRand] = this.puntosValidos[i];
            this.puntosValidos[i] = puntoAnterior;
        }
    }

    private void invocarEdificios() {
        ubicacionesValidas();
        shufflePuntosValidos();
        Random random = new Random();
        int nInvocaciones = random.nextInt(4);//define cuantos edificios hay que invocar
        int nOpcion = (random.nextInt(5) + 1);//escoge un tamaño de edificio al azar
        edificios = new Edificio[4 + nInvocaciones];
        System.out.println("N edif: " + (edificios.length));
        for (int nEdificio = 0; nEdificio < edificios.length; nEdificio++) {
            edificios[nEdificio] = new Edificio(puntosValidos[nEdificio].getX(), puntosValidos[nEdificio].getY(), nOpcion);
            nOpcion = random.nextInt(5) + 1;
        }

    }

    //inicia los puntos que son permitidos para invocar los edificios
    private void ubicacionesValidas() {
        //ZONA 1
        this.puntosValidos[0] = new Point(160, 160);
        this.puntosValidos[1] = new Point(160, 320);
        this.puntosValidos[2] = new Point(160, 480);
        //ZONA 2
        this.puntosValidos[3] = new Point(320, 160);
        this.puntosValidos[4] = new Point(320, 320);
        this.puntosValidos[5] = new Point(320, 480);
        //ZONA3
        this.puntosValidos[6] = new Point(480, 160);
        //------------------------------------
        //this.zonaValidas[-] = new Point(-,-);// SECTOR RESERVADO A LOS JUGADORES
        //------------------------------------
        this.puntosValidos[7] = new Point(480, 448);
        //ZONA 4
        this.puntosValidos[8] = new Point(640, 160);
        this.puntosValidos[9] = new Point(640, 320);
        this.puntosValidos[10] = new Point(640, 480);
        //ZONA 5
        this.puntosValidos[11] = new Point(800, 160);
        this.puntosValidos[12] = new Point(800, 320);
        this.puntosValidos[13] = new Point(800, 480);

    }

    public void dibujarSuelo(Entorno entorno) {
        entorno.dibujarImagen(suelo, entorno.ancho() / 2, entorno.alto() / 2, 0);
    }

    public void dibujarParedesContorno(Entorno entorno) {
        entorno.dibujarImagen(paredesContorno, entorno.ancho() / 2, entorno.alto() / 2, 0);
    }

    //RELLENA LOS EDIFICIOS CON LOS BLOQUES DEL SPRITE
    public void dibujarParedesEdificios(Entorno entorno, int tamBloques) {

        for (int ed = 0; ed < edificios.length; ed++) {
            if (edificios[ed] != null) {
                int bloquesX = (int) edificios[ed].getAncho() / tamBloques;
                int bloquesY = (int) edificios[ed].getAlto() / tamBloques;
                Point[][] posicionFiguras = new Point[bloquesX][bloquesY];
                int puntoX;
                int puntoY;

                for (int columnaY = 0; columnaY < bloquesY; columnaY++) {
                    for (int filaX = 0; filaX < bloquesX; filaX++) {
                        puntoX = (int) ((edificios[ed].getX() - edificios[ed].getAncho() / 2) + tamBloques / 2) + (tamBloques * (filaX));
                        puntoY = (int) ((edificios[ed].getY() - edificios[ed].getAlto() / 2) + tamBloques / 2) + (tamBloques * (columnaY));
                        posicionFiguras[filaX][columnaY] = new Point(puntoX, puntoY);
                    }
                }
                for (int i = 0; i < bloquesY; i++) {
                    for (int j = 0; j < bloquesX; j++) {
                        entorno.dibujarImagen(ladrillosEdificio, posicionFiguras[j][i].getX(), posicionFiguras[j][i].getY(), 0);
                    }
                }
            }
        }
    }

    public Edificio[] getEdificios() {
        return edificios;
    }

}
