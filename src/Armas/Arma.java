/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Armas;

import Enemigo.Enemigo;
import Enemigo.Serpiente;
import Nivel.Edificio;
import entorno.Entorno;
import juego.Exterminador;

public class Arma {

	private Mina mina;
	private Explosion explosion;
	private Pistola pistola;
	private Escopeta escopeta;
	private Domo domo;
	private TelaArania telaArania;

	public Arma() {

	}

	public void dibujar(Entorno e) {
		if (pistola != null) {
			pistola.dibujar(e);
		}
		if (escopeta != null) {
			escopeta.dibujar(e);
		}
		if (mina != null) {
			mina.dibujar(e);
		}
		if (explosion != null) {
			explosion.dibujar(e);
		}
		if (telaArania != null) {
			telaArania.dibujar(e);
		}
		if (domo != null) {
			domo.dibujar(e);
		}

	}

	public void mover() {
		if (pistola != null) {
			pistola.mover();
		}
		if (escopeta != null) {
			escopeta.mover();
		}

		if (telaArania != null) {
			telaArania.mover();
		}
	}

	// ESTADOS DE LAS ARMAS - ALGUNAS ACCIONES AFECTAN EL COMPORTAMIENTO DEL ARMA
	public Arma estadosDelArma(Entorno e, Edificio[] ed, Enemigo[] en, Arma[] arm, Exterminador[] ex,Serpiente s) {

		if (pistolaCochoCon(e, ed, en, arm,s) == null) {
			return null;
		}
		if (escopetaCochoCon(e, ed, en, arm,s) == null) {
			return null;
		}
		if (minaCochoCon(e, ed, en, arm, ex) == null) {
			return null;
		}
		if (explosionCochoCon(e, en, arm, ex) == null) {
			return null;
		}
		if (telaAraniaCochoCon(e, ed, en, arm, ex) == null) {
			return null;
		}
		if (domoCochoCon(e, ed, en, arm, ex) == null) {
			return null;
		}
		return duracion();
	}

	// AFECTA LA TRAYECTORIA DE LOS PROYECTILES Y LA DURACION DE LAS EXPLOSIONES
	private Arma duracion() {
		if (explosion != null) {
			if (explosion.isMuerto()) {
				return null;
			}
		}
		if (pistola != null) {
			if (pistola.isMuerto()) {
				return null;
			}
		}
		if (telaArania != null) {
			if (telaArania.isMuerto()) {
				return null;
			}
		}
		if (escopeta != null) {
			if (escopeta.isMuerto()) {
				return null;
			}
		}
		if (mina != null) {
			if (mina.isExplotar()) {
				explotarMina();
			}
		}
		if (domo != null) {
			if (domo.isMuerto()) {
				return null;
			}
		}

		return this;
	}

	// COLISIONES INDIVIDUALIZADAS - AGREGAR METODOS INDIVIDUALES DE COLISION ACA
	private Arma pistolaCochoCon(Entorno e, Edificio[] ed, Enemigo[] en, Arma[] arm,Serpiente s) {
		if (pistola != null) {

			if (pistola.chocasteCon(e)) {
				System.out.println("Choco entorno");
				return null;
			}

			if (pistola.chocasteCon(ed)) {
				System.out.println("Choco edificio");
				return null;
			}

			if (pistola.chocasteCon(en)) {
				System.out.println("Choco enemigo");
				
				return null;
			}

			if (pistola.chocasteCon(arm)) {
				System.out.println("Choco Arma");
				return null;
			}
			
			if (pistola.chocasteCon(s)) {
				System.out.println("Choco Serpiente");
				return null;
			}
		}
		return this;
	}

	private Arma escopetaCochoCon(Entorno e, Edificio[] ed, Enemigo[] en, Arma[] arm,Serpiente s) {
		if (escopeta != null) {
			if (escopeta.chocasteCon(e)) {
				System.out.println("Choco entorno");
				return null;
			}

			if (escopeta.chocasteCon(ed)) {
				System.out.println("Choco edificio");
				return null;
			}

			if (escopeta.chocasteCon(en)) {
				System.out.println("Choco enemigo");
				return null;
			}

			if (escopeta.chocasteCon(arm)) {
				System.out.println("Choco Arma");
				return null;
			}
			
			if (escopeta.chocasteCon(s)) {
				System.out.println("Choco Serpiente");
				return null;
			}
		}
		return this;
	}

	private Arma minaCochoCon(Entorno e, Edificio[] ed, Enemigo[] en, Arma[] arm, Exterminador[] ex) {
		if (mina != null) {
			if (mina.chocasteCon(e)) {
				System.out.println("Choco entorno");
				return null;
			}
			if (mina.chocasteCon(ed)) {
				System.out.println("Choco edificio");
				return null;
			}
			if (mina.chocasteCon(en)) {
				System.out.println("Choco enemigo");
				mina.explotar(true);
				return this;
			}
			if (mina.chocasteCon(arm)) {
				System.out.println("Choco ARMA");
				mina.explotar(true);
				return this;
			}
		}
		return this;
	}

	private void explotarMina() {
		if (this.mina.isExplotar()) {
			if (mina.contarTiempo()) {
				this.explosion = new Explosion(mina.getX(), mina.getY(), 0);
				this.mina = null;
			}
		}
	}

	private Arma explosionCochoCon(Entorno e, Enemigo[] en, Arma[] arm, Exterminador[] ex) {
		if (explosion != null) {
			if (explosion.chocasteCon(e)) {
				System.out.println("Choco entorno");
				return null;
			}
			if (explosion.chocasteCon(en)) {
				System.out.println("Choco enemigo");
				return this;
			}
			if (explosion.chocasteCon(ex)) {
				System.out.println("Choco EXTERMINADOR");
				return this;
			}
		}
		return this;
	}

	private Arma telaAraniaCochoCon(Entorno e, Edificio[] ed, Enemigo[] en, Arma[] arm, Exterminador[] ex) {
		if (telaArania != null) {

			if (telaArania.chocasteCon(e)) {
				System.out.println("Choco entorno");
				return null;
			}

			if (telaArania.chocasteCon(ed)) {
				System.out.println("Choco edificio");
				return null;
			}

			if (telaArania.chocasteCon(arm)) {
				System.out.println("Choco Arma");
				return null;
			}

			if (telaArania.chocasteCon(ex)) {
				System.out.println("Choco TelaArania");
				return this;
			}
		}
		return this;
	}

	private Arma domoCochoCon(Entorno e, Edificio[] ed, Enemigo[] en, Arma[] arm, Exterminador[] ex) {
		if (domo != null) {
			if (domo.chocasteCon(en)) {
				System.out.println("Choco enemigo");
				return this;

			}
		}

		return this;
	}

	public void setPistola(double x, double y, double angulo) {
		this.pistola = new Pistola(x, y, angulo);
	}

	public void setEscopeta(double x, double y, double angulo) {
		this.escopeta = new Escopeta(x, y, angulo);
	}

	public void setMina(double x, double y, double angulo) {
		this.mina = new Mina(x, y, angulo);
	}

	public void setTelaArania(double x, double y, double angulo) {
		this.telaArania = new TelaArania(x, y, angulo);
	}

	public void setDomo(double x, double y) {
		this.domo = new Domo(x, y);
	}
        
        public void setExplosion(double x, double y) {
		this.explosion = new Explosion(x, y,0);
	}

	public Mina getMina() {
		return mina;
	}

	public Pistola getPistola() {
		return pistola;
	}

	public Escopeta getEscopeta() {
		return escopeta;
	}

	public Explosion getExplosion() {
		return explosion;
	}

	public TelaArania getTelaArania() {
		return telaArania;
	}
	
	public Domo domo() {
		return domo;
	}
}
