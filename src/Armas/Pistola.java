/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Armas;

import entorno.Herramientas;
import entorno.Entorno;
import java.awt.Image;

import javax.sound.sampled.LineUnavailableException;

import Enemigo.Enemigo;
import Enemigo.Serpiente;
import Nivel.Edificio;

/**
 *
 * @author Matt
 */
public class Pistola {

	private double x;
	private double y;
	private double xOrigen;
	private double yOrigen;
	private double angulo;
	private String nombre;
	private Image sprites;
	private double danio;

	private double velocidad;
	private double radio;

	private boolean muerto;

	public Pistola(double x, double y, double angulo) {
		this.angulo = angulo;
		this.x = Math.cos(angulo + (9 * Math.PI / 180)) * 50 + x;
		this.y = Math.sin(angulo + (9 * Math.PI / 180)) * 50 + y;
		this.xOrigen = x;
		this.yOrigen = y;
		this.nombre = "Pistola"; // PISTOLA
		this.sprites = Herramientas.cargarImagen(this.nombre + ".png");
		this.danio = 2;
		this.velocidad = 20;
		this.radio = 5;
	}

	public void dibujar(Entorno e) {
		e.dibujarImagen(sprites, x, y, angulo, 1.5);
		distanciaRecorrida();
	}

	public void distanciaRecorrida() {
		double ady = x - xOrigen;
		double op = y - yOrigen;
		double hyp = Math.sqrt(op * op + ady * ady);
		if (hyp > 500) {
			this.muerto = true;
		}
	}

	public boolean chocasteCon(Entorno e) {
		return (x > e.ancho() || x < 0 || y < 0 || y > e.alto());
	}

	public boolean chocasteCon(Edificio[] edificio) {
		for (int i = 0; i < edificio.length; i++) {
			if (edificio[i] != null) {
				if ((x + radio >= edificio[i].getX() - edificio[i].getAncho() / 2
						&& x - radio <= edificio[i].getX() + edificio[i].getAncho() / 2)
						&& (y + radio >= edificio[i].getY() - edificio[i].getAlto() / 2
								&& y - radio <= edificio[i].getY() + edificio[i].getAlto() / 2)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean chocasteCon(Serpiente serpiente) {
		if (serpiente != null) {
			for (int i = serpiente.getNodoCola(); i < serpiente.getNodosSerpiente().length; i++) {
				if (serpiente.getNodosSerpiente()[i] != null) {
					if ((x + radio >= serpiente.getNodosSerpiente()[i].getX()
							- serpiente.getNodosSerpiente()[i].getRadio()
							&& x - radio <= serpiente.getNodosSerpiente()[i].getX()
									+ serpiente.getNodosSerpiente()[i].getRadio())
							&& (y + radio >= serpiente.getNodosSerpiente()[i].getY()
									- serpiente.getNodosSerpiente()[i].getRadio()
									&& y - radio <= serpiente.getNodosSerpiente()[i].getY()
											+ serpiente.getNodosSerpiente()[i].getRadio())) {
						if (i == serpiente.getNodoCola()) {
							serpiente.getNodosSerpiente()[i] = null;
							try {
								serpiente.daniado();
								;
							} catch (LineUnavailableException e) {
							}
							serpiente.setGolpeado(true);
							System.out.println("me dolio");
						}
						return true;
					}
				}
			}

		}
		return false;
	}

	public boolean chocasteCon(Enemigo[] enemigo) {
		for (int i = 0; i < enemigo.length; i++) {
			if (enemigo[i] != null) {
				if ((x + radio >= enemigo[i].getX() - enemigo[i].getRadio())
						&& (x - radio <= enemigo[i].getX() + enemigo[i].getRadio())
						&& (y + radio >= enemigo[i].getY() - enemigo[i].getRadio())
						&& (y - radio <= enemigo[i].getY() + enemigo[i].getRadio())) {
					enemigo[i].quitarVida(danio);
					try {
						enemigo[i].daniado();
					} catch (LineUnavailableException e) {
					}
					return true;
				}
			}
		}
		return false;
	}

	public boolean chocasteCon(Arma[] arma) {
		for (int i = 0; i < arma.length; i++) {
			if (arma[i] != null) {
				// AGREGANDO -> arma[i].getPistola()!=this //EVITA QUE CHOQUE CONSIGO MISMO
				if (arma[i].getPistola() != null && arma[i].getPistola() != this) {
					if ((x + radio >= arma[i].getPistola().getX() - arma[i].getPistola().getRadio()
							&& x - radio <= arma[i].getPistola().getX() + arma[i].getPistola().getRadio())
							&& (y + radio >= arma[i].getPistola().getY() - arma[i].getPistola().getRadio()
									&& y - radio <= arma[i].getPistola().getY() + arma[i].getPistola().getRadio())) {
						return true;
					}
				}

				if (arma[i].getEscopeta() != null) {
					if ((x + radio >= arma[i].getEscopeta().getX() - arma[i].getEscopeta().getRadio()
							&& x - radio <= arma[i].getEscopeta().getX() + arma[i].getEscopeta().getRadio())
							&& (y + radio >= arma[i].getEscopeta().getY() - arma[i].getEscopeta().getRadio()
									&& y - radio <= arma[i].getEscopeta().getY() + arma[i].getEscopeta().getRadio())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public void mover() {
		x += velocidad * Math.cos(angulo);
		y += velocidad * Math.sin(angulo);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getRadio() {
		return radio;
	}

	public boolean isMuerto() {
		return muerto;
	}

	public void setAngulo(double angulo) {
		this.angulo = angulo;
	}

	public double getDanio() {
		return danio;
	}

}
