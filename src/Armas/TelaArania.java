package Armas;

import entorno.Herramientas;
import juego.Exterminador;
import entorno.Entorno;
import java.awt.Color;
import java.awt.Image;

import Nivel.Edificio;

public class TelaArania {

    private double x;
    private double y;
    private double xOrigen;
    private double yOrigen;
    private double angulo;
    private String nombre;
    private Image sprites;
    private double danio;
    private double reducirVelocidad;

    private double velocidad;
    private double radio;

    private long tiempoAnterior;
    private double tiempoDeVida;
    private double tiempoConVida;
    private double velocidadFrame;
    private boolean muerto;

    public TelaArania(double x, double y, double angulo) {
        this.angulo = angulo;
        this.x = Math.cos(angulo + (9 * Math.PI / 180)) * 50 + x;
        this.y = Math.sin(angulo + (9 * Math.PI / 180)) * 50 + y;
        this.xOrigen = x;
        this.yOrigen = y;
        this.nombre = "Telarania"; // PISTOLA
        this.sprites = Herramientas.cargarImagen(this.nombre + ".png");
        this.danio = 0;
        this.reducirVelocidad = 0.9;//reduce 10%
        this.tiempoDeVida = 2;
        this.velocidadFrame = 0.2;
        this.velocidad = 5;
        this.radio = 20;
    }

    public void dibujar(Entorno e) {
        e.dibujarImagen(sprites, x, y, angulo,0.25);
        distanciaRecorrida();
    }

    public void contarTiempo() {
        long timeAct = System.currentTimeMillis() / 100;
        if (timeAct > tiempoAnterior + this.velocidadFrame) {
            if (this.tiempoConVida >= this.tiempoDeVida) {
                this.muerto = true;
            } else {
                this.tiempoConVida++;
            }
            tiempoAnterior = timeAct;
        }
    }

    public void distanciaRecorrida() {
        double ady = x - xOrigen;
        double op = y - yOrigen;
        double hyp = Math.sqrt(op * op + ady * ady);
        if (hyp > 100) {
            this.velocidad = 0;
        }
    }

    public boolean chocasteCon(Entorno e) {
        return (x > e.ancho() || x < 0 || y < 0 || y > e.alto());
    }

    public boolean chocasteCon(Edificio[] edificio) {
        for (int i = 0; i < edificio.length; i++) {
            if (edificio[i] != null) {
                if ((x + radio >= edificio[i].getX() - edificio[i].getAncho() / 2
                        && x - radio <= edificio[i].getX() + edificio[i].getAncho() / 2)
                        && (y + radio >= edificio[i].getY() - edificio[i].getAlto() / 2
                        && y - radio <= edificio[i].getY() + edificio[i].getAlto() / 2)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean chocasteCon(Exterminador[] exterminador) {
        for (int i = 0; i < exterminador.length; i++) {
            if (exterminador[i] != null) {
                if ((x + radio >= exterminador[i].getX() - (exterminador[i].getBase() / 2)
                        && x - radio <= exterminador[i].getX() + (exterminador[i].getBase() / 2)
                        && y + radio >= exterminador[i].getY() - (exterminador[i].getAltura() / 2)
                        && y - radio <= exterminador[i].getY() + (exterminador[i].getAltura() / 2))) {
                    exterminador[i].setVelocidad(exterminador[i].getVelocidadMaxima() * 0.5);
                    return true;
                }
            }
        }
        return false;

    }

    public boolean chocasteCon(Arma[] arma) {
        for (int i = 0; i < arma.length; i++) {
            if (arma[i] != null) {
                // AGREGANDO -> arma[i].getPistola()!=this //EVITA QUE CHOQUE CONSIGO MISMO
                if (arma[i].getTelaArania() != null && arma[i].getTelaArania() != this) {
                    if ((x + radio >= arma[i].getTelaArania().getX() - arma[i].getTelaArania().getRadio()
                            && x - radio <= arma[i].getTelaArania().getX() + arma[i].getTelaArania().getRadio())
                            && (y + radio >= arma[i].getTelaArania().getY() - arma[i].getTelaArania().getRadio()
                            && y - radio <= arma[i].getTelaArania().getY() + arma[i].getTelaArania().getRadio())) {
                        return true;
                    }
                }

                if (arma[i].getPistola() != null) {
                    if ((x + radio >= arma[i].getPistola().getX() - arma[i].getPistola().getRadio()
                            && x - radio <= arma[i].getPistola().getX() + arma[i].getPistola().getRadio())
                            && (y + radio >= arma[i].getPistola().getY() - arma[i].getPistola().getRadio()
                            && y - radio <= arma[i].getPistola().getY() + arma[i].getPistola().getRadio())) {
                        return true;
                    }
                }

                if (arma[i].getEscopeta() != null) {
                    if ((x + radio >= arma[i].getEscopeta().getX() - arma[i].getEscopeta().getRadio()
                            && x - radio <= arma[i].getEscopeta().getX() + arma[i].getEscopeta().getRadio())
                            && (y + radio >= arma[i].getEscopeta().getY() - arma[i].getEscopeta().getRadio()
                            && y - radio <= arma[i].getEscopeta().getY() + arma[i].getEscopeta().getRadio())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void mover() {
        x += velocidad * Math.cos(angulo);
        y += velocidad * Math.sin(angulo);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getRadio() {
        return radio;
    }

    public boolean isMuerto() {
        return muerto;
    }

    public void setAngulo(double angulo) {
        this.angulo = angulo;
    }
}
