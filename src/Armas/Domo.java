package Armas;

import java.awt.Image;

import Enemigo.Enemigo;
import Nivel.Edificio;
import entorno.Entorno;
import entorno.Herramientas;
import juego.Exterminador;

public class Domo {

    private double x;
    private double y;
    private double radio;
    private String nombre;
    private Image imagenArma;
    private double danio;
    private double tiempoAnterior;
    private double tiempoConVida;
    private double tiempoDeVida;
    private boolean muerto;

    public Domo(double x, double y) {
        this.x = x;
        this.y = y;
        this.nombre = "Domo";
        this.imagenArma = Herramientas.cargarImagen(this.nombre + ".png");
        this.danio = 0;
        this.tiempoDeVida = 200;
        this.radio = 70;
    }

    public boolean contarTiempo() {
        long timeAct = System.currentTimeMillis() / 100;
        if (this.tiempoConVida >= this.tiempoDeVida) {
            muerto = true;
        } else {
            this.tiempoConVida++;
        }
        this.tiempoAnterior = timeAct;
        return false;
    }

    public void dibujar(Entorno e) {
        e.dibujarImagen(imagenArma, x, y, 0, 2);
        contarTiempo();
    }

    public boolean chocasteCon(Edificio[] edificio) {
        for (int i = 0; i < edificio.length; i++) {
            if (edificio[i] != null) {
                if ((x + radio >= edificio[i].getX() - edificio[i].getAncho() / 2
                        && x - radio <= edificio[i].getX() + edificio[i].getAncho() / 2)
                        && (y + radio >= edificio[i].getY() - edificio[i].getAlto() / 2
                        && y - radio <= edificio[i].getY() + edificio[i].getAlto() / 2)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean chocasteCon(Enemigo[] enemigo) {
        for (int i = 0; i < enemigo.length; i++) {
            if (enemigo[i] != null) {
                if ((x + radio >= enemigo[i].getX() - enemigo[i].getRadio())
                        && (x - radio <= enemigo[i].getX() + enemigo[i].getRadio())
                        && (y + radio >= enemigo[i].getY() - enemigo[i].getRadio())
                        && (y - radio <= enemigo[i].getY() + enemigo[i].getRadio())) {
                    enemigo[i].moverAtras();
                }
            }
        }
        return false;
    }

    public boolean chocasteCon(Exterminador[] exterminador) {
        for (int i = 0; i < exterminador.length; i++) {
            if (exterminador[i] != null) {
                if ((x + radio >= exterminador[i].getX() - (exterminador[i].getBase() / 2)
                        && x - radio <= exterminador[i].getX() + (exterminador[i].getBase() / 2)
                        && y + radio >= exterminador[i].getY() - (exterminador[i].getAltura() / 2)
                        && y - radio <= exterminador[i].getY() + (exterminador[i].getAltura() / 2))) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isMuerto() {
        return muerto;
    }

    public double getDanio() {
        return danio;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getRadio() {
        return radio;
    }
    

}
