/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Armas;

import entorno.Entorno;
import entorno.Herramientas;
import juego.Exterminador;

import java.awt.Image;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

import Enemigo.Enemigo;

public class Explosion {

    private double x;
    private double y;
    private double angulo;
    private String nombre;
    private double danio;
    private double radio;

    private Image[] sprites = new Image[8];
    private long tiempoSpriteAnterior;
    private int spriteActual;
    private double tiempoFrame;
    private boolean muerto;
    
    private Clip explota;

    public Explosion(double x, double y, double angulo) {
        this.angulo = angulo;
        this.x = Math.cos(angulo + (9 * Math.PI / 180)) * 50 + x;
        this.y = Math.sin(angulo + (9 * Math.PI / 180)) * 50 + y;
        this.nombre = "Explosion"; // MINA        
        this.cargarSprite();
        this.danio = 1;
        this.tiempoFrame = 0.2;
        this.radio = 100;
        
        this.explota = entorno.Herramientas.cargarSonido("explosion.wav");
        
        if (!explota.isActive()) {
    		explota.setFramePosition(0);
			explota.start();
    	}
    }

    private void cargarSprite() {
        for (int numeroSprite = 0; numeroSprite < sprites.length; numeroSprite++) {
            this.sprites[numeroSprite] = Herramientas.cargarImagen("explosion" + "-0" + (numeroSprite + 1) + ".png");
        }
        this.tiempoSpriteAnterior = 0;
        this.spriteActual = 0;
    }

    public void dibujar(Entorno e) {
    	
        //e.dibujarImagen(spriteActual, x, y, angulo);
        long timeAct = System.currentTimeMillis() / 100;
        e.dibujarImagen(sprites[spriteActual], x, y, 0, 1);
        if (timeAct > tiempoSpriteAnterior + tiempoFrame) {
            if (this.spriteActual >= this.sprites.length - 1) {
                this.spriteActual = 0;
                this.muerto = true;
            } else {
                this.spriteActual++;
            }

            tiempoSpriteAnterior = timeAct;
        }
    }
    
    public void explota() throws LineUnavailableException {
		explota.setFramePosition(0);
		explota.start();
	}

    public boolean chocasteCon(Entorno e) {
        return (x > e.ancho() || x < 0 || y < 0 || y > e.alto());
    }

    public boolean chocasteCon(Arma[] arma) {
        for (int i = 0; i < arma.length; i++) {
            if (arma[i] != null) {
                if (arma[i].getMina() != null) {
                    if ((x + radio >= arma[i].getMina().getX() - arma[i].getMina().getRadio()
                            && x - radio <= arma[i].getMina().getX() + arma[i].getMina().getRadio())
                            && (y + radio >= arma[i].getMina().getY() - arma[i].getMina().getRadio()
                            && y - radio <= arma[i].getMina().getY() + arma[i].getMina().getRadio())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean chocasteCon(Enemigo[] enemigo) {
        for (int i = 0; i < enemigo.length; i++) {
            if (enemigo[i] != null) {
                if ((x + radio >= enemigo[i].getX() - enemigo[i].getRadio())
                        && (x - radio <= enemigo[i].getX() + enemigo[i].getRadio())
                        && (y + radio >= enemigo[i].getY() - enemigo[i].getRadio())
                        && (y - radio <= enemigo[i].getY() + enemigo[i].getRadio())) {
                    enemigo[i].quitarVida(danio);//QUITA LA VIDA AL ENEMIGO   
                    if (!enemigo[i].daniado.isActive()) {
                    	try {
    						enemigo[i].daniado();
    					} catch (LineUnavailableException e) {
    					}
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public boolean chocasteCon(Exterminador[] exterminador) {
        for (int i = 0; i < exterminador.length; i++) {
            if (exterminador[i] != null) {
                if ((x + radio >= exterminador[i].getX() - (exterminador[i].getBase() / 2)
                        && x - radio <= exterminador[i].getX() + (exterminador[i].getBase() / 2)
                        && y + radio >= exterminador[i].getY() - (exterminador[i].getAltura() / 2)
                        && y - radio <= exterminador[i].getY() + (exterminador[i].getAltura() / 2))) {
                    exterminador[i].quitarVida(danio);
                    return true;
                }
            }
        }
        return false;

    }

    public String getNombre() {
        return nombre;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getRadio() {
        return radio;
    }

    public boolean isMuerto() {
        return muerto;
    }

    public double getDanio() {
        return danio;
    }

}
