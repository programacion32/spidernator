
package Armas;

import entorno.Entorno;
import entorno.Herramientas;
import juego.Exterminador;

import java.awt.Image;

import Enemigo.Enemigo;
import Nivel.Edificio;

public class Mina {

    private double x;
    private double y;
    private double angulo;
    private String nombre;
    private Image imagenArma;
    private double danio;
    private double tiempoAnterior;
    private double tiempoConVida;
    private double tiempoDeVida;
    private double velocidadFrame;
    private double radio;
    private boolean explotar;

    public Mina(double x, double y, double angulo) {
        this.angulo = angulo;
        this.x = x;
        this.y = y;
        this.nombre = "Mina";
        this.imagenArma = Herramientas.cargarImagen(this.nombre + ".png");
        this.danio = 2;
        this.tiempoDeVida = 2;
        this.radio = 20;
    }

    public boolean contarTiempo() {
        long timeAct = System.currentTimeMillis() / 100;
        if (timeAct > tiempoAnterior + this.velocidadFrame) {
            if (this.tiempoConVida >= this.tiempoDeVida) {
                return true;
            } else {
                this.tiempoConVida++;
            }
            tiempoAnterior = timeAct;
        }
        return false;
    }

    public void dibujar(Entorno e) {
        e.dibujarImagen(imagenArma, x, y, angulo);
    }

    public boolean chocasteCon(Entorno e) {
        return (x > e.ancho() || x < 0 || y < 0 || y > e.alto());
    }

    public boolean chocasteCon(Edificio[] edificio) {
        for (int i = 0; i < edificio.length; i++) {
            if (edificio[i] != null) {
                if ((x + radio >= edificio[i].getX() - edificio[i].getAncho() / 2
                        && x - radio <= edificio[i].getX() + edificio[i].getAncho() / 2)
                        && (y + radio >= edificio[i].getY() - edificio[i].getAlto() / 2
                        && y - radio <= edificio[i].getY() + edificio[i].getAlto() / 2)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean chocasteCon(Enemigo[] enemigo) {
        for (int i = 0; i < enemigo.length; i++) {
            if (enemigo[i] != null) {
                if ((x + radio >= enemigo[i].getX() - enemigo[i].getRadio())
                        && (x - radio <= enemigo[i].getX() + enemigo[i].getRadio())
                        && (y + radio >= enemigo[i].getY() - enemigo[i].getRadio())
                        && (y - radio <= enemigo[i].getY() + enemigo[i].getRadio())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean chocasteCon(Exterminador[] exterminador) {
        for (int i = 0; i < exterminador.length; i++) {
            if (exterminador[i] != null) {
                if ((x + radio >= exterminador[i].getX() - (exterminador[i].getBase() / 2)
                        && x - radio <= exterminador[i].getX() + (exterminador[i].getBase() / 2)
                        && y + radio >= exterminador[i].getY() - (exterminador[i].getAltura() / 2)
                        && y - radio <= exterminador[i].getY() + (exterminador[i].getAltura() / 2))) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean chocasteCon(Arma[] arma) {
        for (int i = 0; i < arma.length; i++) {
            if (arma[i] != null) {
                if (arma[i].getPistola() != null) {
                    if ((x + radio >= arma[i].getPistola().getX() - arma[i].getPistola().getRadio()
                            && x - radio <= arma[i].getPistola().getX() + arma[i].getPistola().getRadio())
                            && (y + radio >= arma[i].getPistola().getY() - arma[i].getPistola().getRadio()
                            && y - radio <= arma[i].getPistola().getY() + arma[i].getPistola().getRadio())) {
                        return true;
                    }
                }

                if (arma[i].getEscopeta() != null) {
                    if ((x + radio >= arma[i].getEscopeta().getX() - arma[i].getEscopeta().getRadio()
                            && x - radio <= arma[i].getEscopeta().getX() + arma[i].getEscopeta().getRadio())
                            && (y + radio >= arma[i].getEscopeta().getY() - arma[i].getEscopeta().getRadio()
                            && y - radio <= arma[i].getEscopeta().getY() + arma[i].getEscopeta().getRadio())) {
                        return true;
                    }
                }

                if (arma[i].getExplosion() != null) {
                    if ((x + radio >= arma[i].getExplosion().getX() - arma[i].getExplosion().getRadio()
                            && x - radio <= arma[i].getExplosion().getX() + arma[i].getExplosion().getRadio())
                            && (y + radio >= arma[i].getExplosion().getY() - arma[i].getExplosion().getRadio()
                            && y - radio <= arma[i].getExplosion().getY() + arma[i].getExplosion().getRadio())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public String getNombre() {
        return nombre;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getRadio() {
        return radio;
    }

    public boolean isExplotar() {
        return explotar;
    }

    public void explotar(boolean explotar) {
        this.explotar = explotar;
    }
    
     public double getDanio() {
        return danio;
    }
}
